﻿using System.Diagnostics;
using System.Windows;
using System.Globalization;
using System;
using System.Linq;
using System.IO.IsolatedStorage;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Microsoft.Phone.Scheduler;
using sharedcode.Resources;
using sharedcode.Entities;
using sharedcode.Models;
using sharedcode.Server;
using sharedcode.Server.Responses;
using sharedcode.Helpers;
using System.Net;
using Microsoft.Phone.Shell;

namespace livetile
{
    public class ScheduledAgent : ScheduledTaskAgent
    {
        private readonly static IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

        static ScheduledAgent()
        {
            Deployment.Current.Dispatcher.BeginInvoke(delegate
            {
                Application.Current.UnhandledException += UnhandledException;
            });
        }

        private static void UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (Debugger.IsAttached)
            {
                Debugger.Break();
            }
        }

        protected override async void OnInvoke(ScheduledTask task)
        {
            LogEntries.LogEntriesService.Initialize(null, "b9aa9b91-5a9c-44ba-a30b-3f61ffd668b8", false);

            //LogEntries.LogEntriesService.Log("background agent");

            await SearchJobs();

            await GetMessages();

            NotifyComplete();
        }

        private async Task SearchJobs()
        {
            try
            {
                if (!Api.IsAuthorized) return;

                if (!settings.Contains(Constants.LastSearch)) return;

                string search = (string) settings[Constants.LastSearch];

                ObservableCollection<Job> Jobs = Storage.LoadFromFile<ObservableCollection<Job>>(Constants.RecommendedJobs);

                if (Jobs == null) Jobs = new ObservableCollection<Job>();
            
                SearchJobsResponse searchJobsResponse = await Api.SearchJobs(search);

                //int newJobs = 0;

                if (searchJobsResponse != null && searchJobsResponse.Jobs != null)
                {
                    for (int i = searchJobsResponse.Jobs.Count - 1; i >= 0; i--)
                    {
                        JobResponse jobResponse = searchJobsResponse.Jobs[i];

                        bool found = false;

                        foreach (string skill in jobResponse.SkillsList)
                        {
                            if (CultureInfo.InvariantCulture.CompareInfo.IndexOf(skill.Replace("-", " "), search, CompareOptions.IgnoreCase) != -1)
                            {
                                found = true;
                                break;
                            }
                        }

                        if (found || CultureInfo.InvariantCulture.CompareInfo.IndexOf(jobResponse.Title, search, CompareOptions.IgnoreCase) != -1 || CultureInfo.InvariantCulture.CompareInfo.IndexOf(jobResponse.Snippet, search, CompareOptions.IgnoreCase) != -1)
                        {
                            Job job = Job.Create(jobResponse.Id, null, null, jobResponse.Title, jobResponse.Snippet, jobResponse.SkillsList, jobResponse.Url, jobResponse.Date_created, false);

                            if (!Jobs.Contains(job))
                            {
                                //newJobs++;

                                Jobs.Insert(0, job);
                            }
                        }
                    }

                    int newJobs = Jobs.Count(j => !j.IsSeen);

                    if (newJobs > 0)
                    {
                        TileHelper.UpdateTile(newJobs, Jobs.First().Title, Jobs.First().BriefDescription);

                        Jobs = Jobs.Take(100).OrderByDescending(j => j.Date).ToObservableCollection();

                        Storage.SaveToFile(Constants.RecommendedJobs, Jobs);
                    }
                    else
                    {
                        TileHelper.UpdateTile(0);
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch { }
        }

        private async Task GetMessages()
        {
            if (!Api.IsAuthorized) return;

            string UserName = null;

            if (settings.Contains("UserName"))
            {
                UserName = (string)settings["UserName"];
            }

            ObservableCollection<Conversation> Conversations = Storage.LoadFromFile<ObservableCollection<Conversation>>(Constants.Rooms);

            if (Conversations == null) Conversations = new ObservableCollection<Conversation>();

            try
            {
                if (UserName == null)
                {
                    //GetUserResponse getUserResponse = await Api.GetUserInfo();

                    //if (getUserResponse != null && getUserResponse.AuthUser != null)
                    //{
                    //    UserName = getUserResponse.AuthUser.first_name + " " + getUserResponse.AuthUser.last_name;
                    //}
                    //else
                    //{
                    //    throw new Exception();
                    //}
                }

                Message newMessage = null;
                int newMessages = 0;
                int newMessageId = 0;

                //GetTraysContentResponse getTraysContentResponse = await Api.GetTrayContent(UserName, "");

                //if (getTraysContentResponse != null && getTraysContentResponse.CurrentTray != null && getTraysContentResponse.CurrentTray.threads != null)
                //{
                //    foreach (ThreadResponse threadResponse in getTraysContentResponse.CurrentTray.threads)
                //    {
                //        if (threadResponse.participants != null && threadResponse.participants.Count > 0)
                //        {
                //            ParticipantResponse participantResponse = threadResponse.participants.FirstOrDefault(a => a.username == threadResponse.last_post_sender);

                //            if (participantResponse != null)
                //            {
                //                Message message = new Message()
                //                {
                //                    Id = threadResponse.last_post_id,
                //                    Text = HttpUtility.HtmlDecode(threadResponse.last_post_preview),
                //                    Title = HttpUtility.HtmlDecode(threadResponse.subject),
                //                    Author = HttpUtility.HtmlDecode(participantResponse.first_name + " " + participantResponse.last_name),
                //                    Date = DateTimeHelper.UnixTimeStampToDateTime(threadResponse.last_post_ts),
                //                    IsRead = threadResponse.read == 1,
                //                    Direction = (participantResponse.username == UserName) ? MessageDirection.Outgoing : MessageDirection.Incoming
                //                };

                //                if (!Conversations.Contains(new Conversation() { Id = threadResponse.id }))
                //                {
                //                    ParticipantResponse participantResponse_ = threadResponse.participants.FirstOrDefault(a => a.username != UserName);

                //                    Conversations.Add(new Conversation() { Id = threadResponse.id, Title = HttpUtility.HtmlDecode(threadResponse.subject), Companion = HttpUtility.HtmlDecode(participantResponse_.first_name + " " + participantResponse_.last_name), Messages = new ObservableCollection<Message>() { message } });

                //                    if (message.Direction == MessageDirection.Incoming)
                //                    {
                //                        newMessages++;
                //                        newMessage = message;
                //                        newMessageId = threadResponse.id;
                //                    }
                //                }
                //                else
                //                {
                //                    Conversation conversation = Conversations.FirstOrDefault(c => c.Id == threadResponse.id);

                //                    Message message_ = conversation.Messages.FirstOrDefault(m => m.Id == threadResponse.last_post_id);

                //                    if (message_ == null)
                //                    {
                //                        conversation.Messages.Add(message);

                //                        if (message.Direction == MessageDirection.Incoming)
                //                        {
                //                            newMessages++;
                //                            newMessage = message;
                //                            newMessageId = conversation.Id;
                //                        }
                //                    }

                //                    if (message.IsRead)
                //                    {
                //                        foreach (Message m in conversation.Messages)
                //                        {
                //                            m.IsRead = true;
                //                        }
                //                    }
                //                }
                //            }
                //        }
                //    }

                //    Conversations = Conversations.OrderByDescending(c => c.PreviewMessage.Date).ToObservableCollection();

                //    Storage.SaveToFile(Constants.Conversations, Conversations);

                //    int unreadMessagesCount = 0;
                //    Conversation conversationUnread = null;

                //    foreach (Conversation conversation in Conversations)
                //    {
                //        unreadMessagesCount += (conversation.Messages.Count(m => !m.IsRead && m.Direction == MessageDirection.Incoming) > 0) ? 1 : 0;

                //        if (unreadMessagesCount > 0 && conversationUnread == null)
                //        {
                //            conversationUnread = conversation;
                //        }
                //    }

                //    if (conversationUnread != null)
                //    {
                //        TileHelper.UpdateTile(unreadMessagesCount, conversationUnread.Companion, conversationUnread.PreviewMessage.BriefText);
                //    }

                //    if (newMessage != null)
                //    {
                //        ShellToast shellToast = new ShellToast();

                //        if (newMessages == 1)
                //        {
                //            shellToast.Title = newMessage.Author;
                //            shellToast.Content = newMessage.BriefText;
                //            shellToast.NavigationUri = new Uri("/Views/ConversationPage.xaml?id=" + newMessageId, UriKind.Relative);
                //        }
                //        else
                //        {
                //            shellToast.Title = newMessages + " new messages";
                //            shellToast.Content = "in your inbox";
                //        }

                //        shellToast.Show();
                //    }
                //}
                //else
                //{
                //    throw new Exception();
                //}
            }
            catch
            {
                return;
            }
        }
    }
}
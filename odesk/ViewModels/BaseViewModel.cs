﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows;

namespace odesk.ViewModels
{
    [DataContractAttribute]
    public class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    try
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs(info));
                    }
                    catch
                    { }
                });
            }
        }
    }
}

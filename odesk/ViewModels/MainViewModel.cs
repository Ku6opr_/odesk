﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Windows;
using System.Threading.Tasks;
using System.Net;
using System.Globalization;
using Microsoft.Phone.Scheduler;
using odesk.Helpers;
using sharedcode.Resources;
using sharedcode.Entities;
using sharedcode.Helpers;
using sharedcode.Models;
using sharedcode.Server;
using sharedcode.Server.Responses;
using System.Diagnostics;
using System.Collections.Generic;
using System.Windows.Threading;
using System.Windows.Controls;

namespace odesk.ViewModels
{
    [DataContractAttribute]
    public class MainViewModel : BaseViewModel
    {
        private ObservableCollection<Job> jobsFeed;
        [DataMemberAttribute]
        public ObservableCollection<Job> JobsFeed
        {
            get
            {
                return jobsFeed;
            }
            set
            {
                if (jobsFeed != value)
                {
                    jobsFeed = value;
                    NotifyPropertyChanged("JobsFeed");
                    NotifyPropertyChanged("IsJobsFeedExists");

                    if (Jobs != null)
                    {
                        Jobs.CollectionChanged += (s, e) =>
                        {
                            NotifyPropertyChanged("IsJobsFeedExists");
                        };
                    }
                }
            }
        }

        public bool IsJobsFeedExists
        {
            get
            {
                return ((JobsFeed == null || JobsFeed.Count == 0) && IsLookingForJobsExists) ? false : true;
            }
        }

        private ObservableCollection<string> lookingForJobs;
        [DataMemberAttribute]
        public ObservableCollection<string> LookingForJobs
        {
            get
            {
                return lookingForJobs;
            }
            set
            {
                if (lookingForJobs != value)
                {
                    lookingForJobs = value;
                    NotifyPropertyChanged("LookingForJobs");
                    NotifyPropertyChanged("IsLookingForJobsExists");
                    NotifyPropertyChanged("IsJobsFeedExists");

                    if (LookingForJobs != null)
                    {
                        LookingForJobs.CollectionChanged += (s, e) =>
                        {
                            NotifyPropertyChanged("IsLookingForJobsExists");
                            NotifyPropertyChanged("IsJobsFeedExists");
                        };
                    }
                }
            }
        }

        public bool IsLookingForJobsExists
        {
            get
            {
                return (LookingForJobs == null || LookingForJobs.Count == 0) ? false : true;
            }
        }

        private ObservableCollection<Job> jobs;
        [DataMemberAttribute]
        public ObservableCollection<Job> Jobs
        {
            get
            {
                return jobs;
            }
            set
            {
                if (jobs != value)
                {
                    jobs = value;
                    NotifyPropertyChanged("Jobs");
                    NotifyPropertyChanged("IsJobsExists");

                    if (Jobs != null)
                    {
                        Jobs.CollectionChanged += (s, e) =>
                        {
                            NotifyPropertyChanged("IsJobsExists");
                        };
                    }
                }
            }
        }

        private bool isSearchPerformed;
        [DataMemberAttribute]
        public bool IsSearchPerformed
        {
            get
            {
                return isSearchPerformed;
            }
            set
            {
                if (isSearchPerformed != value)
                {
                    isSearchPerformed = value;
                    NotifyPropertyChanged("IsSearchPerformed");
                }
            }
        }

        public bool IsJobsExists
        {
            get
            {
                return ((Jobs == null || Jobs.Count == 0) && !IsJobSearched) ? false : true;
            }
        }

        private bool isJobSearched;
        [DataMemberAttribute]
        public bool IsJobSearched
        {
            get
            {
                return isJobSearched;
            }
            set
            {
                if (isJobSearched != value)
                {
                    isJobSearched = value;
                    NotifyPropertyChanged("IsJobSearched");
                    NotifyPropertyChanged("IsJobsExists");
                }
            }
        }

        private ObservableCollection<Message> messages;
        [DataMemberAttribute]
        public ObservableCollection<Message> Messages
        {
            get
            {
                return messages;
            }
            set
            {
                if (messages != value)
                {
                    messages = value;
                    NotifyPropertyChanged("Messages");
                    NotifyPropertyChanged("IsMessagesExists");

                    if (Messages != null)
                    {
                        Messages.CollectionChanged += (s, e) =>
                        {
                            NotifyPropertyChanged("IsMessagesExists");
                        };
                    }
                }
            }
        }

        public bool IsMessagesExists
        {
            get
            {
                return ((Messages == null || Messages.Count == 0) && !IsMessageSearched) ? false : true;
            }
        }

        private bool isMessageSearched;
        [DataMemberAttribute]
        public bool IsMessageSearched
        {
            get
            {
                return isMessageSearched;
            }
            set
            {
                if (isMessageSearched != value)
                {
                    isMessageSearched = value;
                    NotifyPropertyChanged("IsMessageSearched");
                    NotifyPropertyChanged("IsMessagesExists");
                }
            }
        }

        public bool IsLastJobExists
        {
            get
            {
                return App.settings.Contains(Constants.LastSearch);
            }
        }

        private ObservableCollection<string> recentSearches;
        [DataMemberAttribute]
        public ObservableCollection<string> RecentSearches//недавние поиски
        {
            get
            {
                return recentSearches;
            }
            set
            {
                if (recentSearches != value)
                {
                    recentSearches = value;
                    NotifyPropertyChanged("RecentSearches");
                    NotifyPropertyChanged("IsRecentSearchesExists");

                    if (RecentSearches != null)
                    {
                        RecentSearches.CollectionChanged += (s, e) =>
                        {
                            NotifyPropertyChanged("IsRecentSearchesExists");
                        };
                    }
                }
            }
        }

        public bool IsRecentSearchesExists
        {
            get
            {
                return (RecentSearches == null || RecentSearches.Count == 0) ? false : true;
            }
        }

        public string LastSearch
        {
            get
            {
                if (App.settings.Contains(Constants.LastSearch))
                {
                    return (string)App.settings[Constants.LastSearch];
                }

                return "";
            }
            set
            {
                if (value != null)
                {
                    App.settings[Constants.LastSearch] = value;
                }
                else
                {
                    App.settings.Remove(Constants.LastSearch);
                }
                NotifyPropertyChanged("LastSearch");
                NotifyPropertyChanged("IsLastJobExists");
                NotifyPropertyChanged("IsJobsExists");
            }
        }

        //[DataMemberAttribute]
        //public int MessagesTrayId { get; set; }

        private ObservableCollection<Conversation> conversations;
        [DataMemberAttribute]
        public ObservableCollection<Conversation> Conversations
        {
            get
            {
                return conversations;
            }
            set
            {
                if (conversations != value)
                {
                    conversations = value;
                    NotifyPropertyChanged("Conversations");
                    NotifyPropertyChanged("IsConversationsExists");

                    if (Conversations != null)
                    {
                        Conversations.CollectionChanged += (s, e) =>
                        {
                            NotifyPropertyChanged("IsConversationsExists");
                        };
                    }
                }
            }
        }

        public bool IsConversationsExists
        {
            get
            {
                return ((Conversations == null || Conversations.Count == 0) && !IsRoomsLoading) ? false : true;
            }
        }

        private bool isRoomsLoading = false;
        [DataMemberAttribute]
        public bool IsRoomsLoading
        {
            get
            {
                return isRoomsLoading;
            }
            set
            {
                if (isRoomsLoading != value)
                {
                    isRoomsLoading = value;
                    NotifyPropertyChanged("IsRoomsLoading");
                    NotifyPropertyChanged("IsRoomsExists");
                }
            }
        }

        //public int ConversationsCount
        //{
        //    get
        //    {
        //        return (Messages != null) ? Messages.Count(m => m.IsRead == false) : 0;
        //    }
        //}

        private Conversation conversation;
        [DataMemberAttribute]
        public Conversation Conversation//разговор
        {
            get
            {
                return conversation;
            }
            set
            {
                if (conversation != value)
                {
                    conversation = value;
                    NotifyPropertyChanged("Conversation");
                }
            }
        }

        private Job job;
        [DataMemberAttribute]
        public Job Job
        {
            get
            {
                return job;
            }
            set
            {
                if (job != value)
                {
                    job = value;
                    NotifyPropertyChanged("Job");
                }
            }
        }

        //private string username;
        //public string UserName
        //{
        //    get
        //    {
        //        if (username != null)
        //        {
        //            return username;
        //        }

        //        if (App.settings.Contains("UserName"))
        //        {
        //            username = (string)App.settings["UserName"];

        //            return username;
        //        }

        //        return null;
        //    }
        //    set
        //    {
        //        username = value;
        //        App.settings["UserName"] = username;
        //        App.settings.Save();
        //    }
        //}

        private User user;
        [DataMemberAttribute]
        public User User
        {
            get
            {
                if (user != null)
                {
                    return user;
                }

                if (App.settings.Contains("User"))
                {
                    user = (User)App.settings["User"];

                    return user;
                }

                return null;
            }
            set
            {
                user = value;
                App.settings["User"] = user;
                App.settings.Save();
            }
        }

        private ObservableCollection<sharedcode.Models.Room> rooms;
        [DataMemberAttribute]
        public ObservableCollection<sharedcode.Models.Room> Rooms
        {
            get
            {
                return rooms;
            }
            set
            {
                if (rooms != value)
                {
                    rooms = value;
                    NotifyPropertyChanged("Rooms");
                    NotifyPropertyChanged("IsRoomsExists");

                    if (Rooms != null)
                    {
                        Rooms.CollectionChanged += (s, e) =>
                        {
                            NotifyPropertyChanged("Rooms");
                            NotifyPropertyChanged("IsRoomsExists");
                        };
                    }
                }
            }
        }

        public bool IsRoomsExists
        {
            get
            {
                return ((Rooms == null || Rooms.Count == 0) && !IsRoomsLoading) ? false : true;
            }
        }

        public List<string> LoadingRooms = new List<string>();

        private ObservableCollection<sharedcode.Models.Room> searchMessagesResult;
        public ObservableCollection<sharedcode.Models.Room> SearchMessagesResult
        {
            get
            {
                return searchMessagesResult;
            }
            set
            {
                searchMessagesResult = value;
                NotifyPropertyChanged("SearchMessagesResult");
                NotifyPropertyChanged("IsSearchMessagesExists");


                SearchMessagesResult.CollectionChanged += (s, e) =>
                {
                    NotifyPropertyChanged("SearchMessagesResult");
                    NotifyPropertyChanged("IsSearchMessagesExists");
                };
            }
        }

        public bool IsSearchMessagesExists
        {
            get
            {
                return (SearchMessagesResult == null || SearchMessagesResult.Count == 0) ? false : true;
            }
        }

        public MainViewModel()
        {
            Conversations = new ObservableCollection<Conversation>();
            Jobs = new ObservableCollection<Job>();
            RecentSearches = new ObservableCollection<string>();
            LookingForJobs = new ObservableCollection<string>();
            JobsFeed = new ObservableCollection<Job>();
            Messages = new ObservableCollection<Message>();

            Rooms = new ObservableCollection<sharedcode.Models.Room>();
            SearchMessagesResult = new ObservableCollection<sharedcode.Models.Room>();

            if (!DesignerProperties.IsInDesignTool)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    LoadRecommendedJobs();
                    LoadConversations();

                    StartBackgroundAgent();

                    string lastSearch = LastSearch;

                    if (!String.IsNullOrWhiteSpace(lastSearch))
                    {
                        Search(lastSearch);
                    }
                });
            }

            loadTimer.Tick += loadTimer_Tick;
            loadTimer.Interval = TimeSpan.FromMilliseconds(1000);
        }

        public void Deactivated()
        {
            foreach (Job job in Jobs)
            {
                job.IsSeen = true;
            }

            SaveRecommendedJobs();

            int unreadMessagesCount = 0;
            Conversation conversationUnread = null;

            foreach (Conversation conversation in Conversations)
            {
                unreadMessagesCount += (conversation.Messages.Count(m => !m.IsRead && m.Direction == MessageDirection.Incoming) > 0) ? 1 : 0;

                if (unreadMessagesCount > 0 && conversationUnread == null)
                {
                    conversationUnread = conversation;
                }
            }

            if (conversationUnread != null)
            {
                TileHelper.UpdateTile(unreadMessagesCount, Conversations.First().Companion, Conversations.First().PreviewMessage.BriefText);
            }
            else
            {
                TileHelper.UpdateTile(0);
            }
        }

        public void SaveRecommendedJobs()
        {
            Storage.SaveToFile(Constants.RecommendedJobs, Jobs);
        }

        private void LoadRecommendedJobs()
        {
            ObservableCollection<Job> jobs = Storage.LoadFromFile<ObservableCollection<Job>>(Constants.RecommendedJobs);

            if (jobs == null) jobs = new ObservableCollection<Job>();

            Jobs = jobs;
        }

        public void SortRecommendedJobs()
        {
            Jobs = Jobs.OrderByDescending(j => j.Date).ToObservableCollection();
        }

        public void LoadConversations()
        {
            ObservableCollection<Conversation> conversations = Storage.LoadFromFile<ObservableCollection<Conversation>>(Constants.Rooms);

            if (conversations == null) conversations = new ObservableCollection<Conversation>();

            Conversations = conversations;
        }

        public void SortConversations()
        {
            Conversations = Conversations.OrderByDescending(c => c.PreviewMessage.Date).ToObservableCollection();
        }

        public async void Search(string search)
        {
            //return;
            if (!Api.IsAuthorized) return;

            //if (!RecentSearches.Contains(search))
            //{
            //    RecentSearches.Add(search);
            //}

            bool isJobsSeen = false;

            LastSearch = search;

            if (Jobs.Count == 0)
            {
                IsJobSearched = true;
                isJobsSeen = true;
                Jobs.Clear();
            }

            try
            {
                SearchJobsResponse searchJobsResponse = await Api.SearchJobs(search);

                if (searchJobsResponse != null && searchJobsResponse.Jobs != null)
                {
                    //Jobs.Clear();                    

                    for (int i = searchJobsResponse.Jobs.Count - 1; i >= 0; i--)
                    {
                        JobResponse jobResponse = searchJobsResponse.Jobs[i];

                        bool found = false;

                        foreach (string skill in jobResponse.SkillsList)
                        {
                            if (CultureInfo.InvariantCulture.CompareInfo.IndexOf(skill.Replace("-", " "), search, CompareOptions.IgnoreCase) != -1)
                            {
                                found = true;
                                break;
                            }
                        }

                        if (found || CultureInfo.InvariantCulture.CompareInfo.IndexOf(jobResponse.Title, search, CompareOptions.IgnoreCase) != -1 || CultureInfo.InvariantCulture.CompareInfo.IndexOf(jobResponse.Snippet, search, CompareOptions.IgnoreCase) != -1)
                        {
                            Job job = Job.Create(jobResponse.Id, null, null, jobResponse.Title, jobResponse.Snippet, jobResponse.SkillsList, jobResponse.Url, jobResponse.Date_created, isJobsSeen);

                            if (!Jobs.Contains(job))
                            {
                                Jobs.Insert(0, job);
                            }
                        }
                    }

                    Jobs = Jobs.Take(100).ToObservableCollection();

                    SortRecommendedJobs();

                    SaveRecommendedJobs();

                    IsJobSearched = false;
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                IsJobSearched = false;

                if (ex.Message.Contains("Unauthorized"))
                {
                    LastSearch = null;
                    System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        App.RootFrame.Navigate(new Uri("/Views/OAuthPage.xaml", UriKind.RelativeOrAbsolute));
                    });
                }
                else
                {
                    //ErrorHelper.ShowErrorMessage(ex.InnerException.Message);
                }
            }
        }

        public void SearchMessages(string search, double frameWidth = 0.0)
        {
            IsMessageSearched = true;
            SearchMessagesResult.Clear();

            try
            {
                if (!String.IsNullOrEmpty(search))
                {
                    var messages = App.ViewModel.Rooms.Where(p => p.Name != null && !String.IsNullOrEmpty(p.Topic) && p.Topic.ToLower().Contains(search.ToLower())).ToList();

                    if (messages != null && messages.Count() > 0)
                    {
                        TextBlock textBlock = new TextBlock();
                        textBlock.Style = (Style)Application.Current.Resources["PhoneTextNormalStyle"];

                        foreach (var item in messages)
                        {
                            textBlock.Text = item.Topic;

                            double textWidth = textBlock.ActualWidth;

                            var searchText = highlightedString(search, item.Topic, frameWidth, textWidth);

                            item.FirstTitle = searchText.FirstText;
                            int startIndex = item.Topic.IndexOf(search);

                            item.SearchWord = (startIndex >= 0) ? item.Topic.Substring(item.Topic.IndexOf(search), search.Length) : search;
                            item.LastTitle = searchText.LastText;
                        }

                        SearchMessagesResult = new ObservableCollection<Room>(messages);
                    }
                }
                else
                {
                    foreach (var item in Rooms)
                    {
                        item.SearchWord = String.Empty;
                    }

                    SearchMessagesResult = new ObservableCollection<Room>(Rooms);
                }

                IsMessageSearched = false;
            }
            catch (Exception ex)
            {
                IsMessageSearched = false;

                //ErrorHelper.ShowErrorMessage(ex.InnerException.Message);
            }
        }

        private SearchText highlightedString(string search, string _string, double frameWidth, double textWidth)
        {
            SearchText searchText = new SearchText();

            int index = _string.ToLower().IndexOf(search.ToLower());

            int trimLeft = 0;

            if (textWidth > frameWidth)
            {
                double charWidth = textWidth / _string.Length;

                if (index * charWidth < frameWidth/2)
                {

                }
                else
                {
                    trimLeft = (Convert.ToInt32(index - frameWidth / 2.0 / charWidth + 4.0));

                    int indexFirstWord = _string.IndexOf(" ", trimLeft);

                    if (trimLeft <= indexFirstWord)
                    {
                        var firstWord = _string.Substring(trimLeft, (indexFirstWord - trimLeft));

                        if (firstWord.Length > 0 && indexFirstWord < index)
                        {
                            if (_string.Length > indexFirstWord)
                            {
                                trimLeft = indexFirstWord;
                            }
                        }

                        searchText.FirstText = "... " + _string.Substring(trimLeft, (index - trimLeft));

                        trimLeft -= 4;

                        searchText.LastText = _string.Substring(index + search.Length);

                        return searchText;
                    }
                }
            }

            searchText.FirstText = _string.Substring(0, index);

            searchText.LastText = _string.Substring(index + search.Length);

            return searchText;
        }

        public async void LoadData()
        {
            try
            {
                await GetUserInfo();

                await GetRooms();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Unauthorized"))
                {
                    LastSearch = null;
                    System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        App.RootFrame.Navigate(new Uri("/Views/OAuthPage.xaml", UriKind.RelativeOrAbsolute));
                    });
                }
                else
                {
                    ErrorHelper.ShowErrorMessage(ex.InnerException.Message);
                }
            }
        }

        public async Task GetUserInfo()
        {
            try
            {
                GetUserResponse getUserResponse = await Api.GetUserInfo();

                if (getUserResponse != null && getUserResponse.AuthUser != null)
                {
                    GetUserReference userReferences = await Api.GetUserReference();

                    if (userReferences != null && userReferences.user != null)
                    {
                        User = new User()
                        {
                            First_name = getUserResponse.AuthUser.first_name,
                            Last_name = getUserResponse.AuthUser.last_name,
                            ImageUrl = getUserResponse.Info.portrait_100_img,
                            Email = userReferences.user.email,
                            UpworkName = userReferences.user.id,
                            Is_provider = userReferences.user.is_provider,
                            Profile_key = userReferences.user.profile_key,
                            Public_url = userReferences.user.public_url,
                            Id = userReferences.user.reference,
                            Status = userReferences.user.status
                        };
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Unauthorized"))
                {
                    LastSearch = null;
                    System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        App.RootFrame.Navigate(new Uri("/Views/OAuthPage.xaml", UriKind.RelativeOrAbsolute));
                    });
                }
                else
                {
                    ErrorHelper.ShowErrorMessage(ex.InnerException.Message);
                }
            }
        }

        public async Task GetJobs()
        {
            try
            {
                var jobs = await Api.GetJobs(User.UpworkName);

                if (jobs != null)
                {
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Unauthorized"))
                {
                    LastSearch = null;
                    System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        App.RootFrame.Navigate(new Uri("/Views/OAuthPage.xaml", UriKind.RelativeOrAbsolute));
                    });
                }
                else
                {
                    ErrorHelper.ShowErrorMessage(ex.InnerException.Message);
                }
            }
        }

        public async Task GetRooms()
        {
            if (!Api.IsAuthorized) return;

            if (Rooms.Count == 0)
            {
                IsRoomsLoading = true;
            }

            try
            {
                Rooms.Clear();

                var savedrooms = Storage.LoadFromFile<ObservableCollection<Room>>(Constants.Messages);

                if (savedrooms != null)
                {
                    Rooms = new ObservableCollection<Room>(savedrooms);
                }

                GetRoomsResponse rooms = await Api.GetRooms(User.UpworkName);

                if (rooms != null && rooms.rooms != null)
                {
                    IsRoomsLoading = false;

                    foreach (var room in rooms.rooms)
                    {
                        System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                        dtDateTime = dtDateTime.AddSeconds((long)room.latestStory.updated / 1000).ToLocalTime();

                        var r = Rooms.FirstOrDefault(p => p.Id == room.roomId);

                        if (r == null)
                        {
                            Rooms.Add(new sharedcode.Models.Room()
                            {
                                Id = room.roomId,
                                IsFavorite = room.isFavorite,
                                Name = room.roomName,
                                OrgId = room.orgId,
                                RoomType = room.roomType,
                                Topic = room.topic,
                                Users = room.users,
                                LastUpdated = dtDateTime
                            });
                        }
                        else
                        {
                            if (r.LastUpdated != dtDateTime)
                            {
                                Rooms[Rooms.IndexOf(Rooms.FirstOrDefault(p => p.Id == room.roomId))].IsUpdate = false;
                            }
                        }
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                IsRoomsLoading = false;

                if (ex.Message.Contains("Unauthorized"))
                {
                    LastSearch = null;
                    System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        App.RootFrame.Navigate(new Uri("/Views/OAuthPage.xaml", UriKind.RelativeOrAbsolute));
                    });
                }
                else
                {
                    ErrorHelper.ShowErrorMessage(ex.InnerException.Message);
                }
            }
        }

        public void SaveRooms()
        {
            Storage.SaveToFile(Constants.Rooms, Rooms);
        }

        public void SaveRooms(ObservableCollection<Room> rooms)
        {
            Storage.SaveToFile(Constants.Messages, rooms);
        }

        //private List<RoomStory> GetRoomStories(List<sharedcode.Server.Responses.StoryResponse> stories)
        //{
        //    if (stories != null)
        //    {
        //        List<RoomStory> roomStory = new List<RoomStory>();

        //        foreach (var item in stories)
        //        {
        //            roomStory.Add(new RoomStory()
        //                {
        //                    Created = item.created,
        //                    IsSystemStory = item.isSystemStory,
        //                    Message = item.message,
        //                    StoryId = item.storyId,
        //                    Updated = item.updated,
        //                    UserId = item.userId
        //                });
        //        }

        //        return roomStory;
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}

        DispatcherTimer loadTimer = new DispatcherTimer();
        public void GetRoomInfo()
        {
            if (!loadTimer.IsEnabled)
            {
                loadTimer.Start();
            }
        }

        async void loadTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (LoadingRooms != null && LoadingRooms.Count > 0)
                {
                    loadTimer.Stop();

                    GetRoomsInfo roomInfo = await Api.GetRoomInformation(User.UpworkName, LoadingRooms[0]);

                    if (roomInfo != null && roomInfo.room != null)
                    {
                        string RoomId = LoadingRooms[0];
                        Deployment.Current.Dispatcher.BeginInvoke(() =>
                            {
                                int index = Rooms.IndexOf(Rooms.FirstOrDefault(p => p.Id == RoomId));
                                if (index != -1)
                                {
                                    Rooms[index].Stories = (roomInfo.room.stories != null) ? new RoomStories() { Cursor = roomInfo.room.stories.cursor, Total = roomInfo.room.stories.total, Stories = GetRoomStories(roomInfo.room.stories.stories) } : null;
                                    Rooms[index].IsUpdate = true;

                                    ObservableCollection<Room> savedRooms = new ObservableCollection<Room>(Rooms.Where(p => p.Stories != null).ToList<Room>());

                                    SaveRooms(savedRooms);
                                }
                            });

                        LoadingRooms.Remove(RoomId);
                    }

                    loadTimer.Start();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private List<RoomStory> GetRoomStories(List<RoomInfoStoryResponse> stories)
        {
            if (stories != null)
            {
                List<RoomStory> roomStories = new List<RoomStory>();

                foreach (var item in stories)
                {
                    roomStories.Add(new RoomStory()
                        {
                            Created = item.created,
                            IsSystemStory = item.isSystemStory,
                            Message = item.message,
                            Updated = item.updated,
                            StoryId = item.storyId,
                            UserId = item.userId
                        });
                }

                return roomStories;
            }
            else
            {
                return null;
            }
        }

        public async Task GetMessages(int threadId)
        {
            if (!Api.IsAuthorized) return;

            //Conversation.Messages.Clear();
            //if (Conversation.Messages.Count == 0)
            //{
            Conversation.IsMessagesLoading = true;
            //}

            try
            {
                GetMessagesResponse getMessagesResponse = await Api.GetMessages(User.FullName, threadId);

                //await Task.Delay(1500);

                Conversation.IsMessagesLoading = false;

                if (getMessagesResponse != null && getMessagesResponse.thread != null && getMessagesResponse.thread.posts != null)
                {
                    //Conversation.Messages.Clear();

                    foreach (PostResponse postResponse in getMessagesResponse.thread.posts)
                    {
                        ParticipantResponse participantResponse = getMessagesResponse.thread.participants.FirstOrDefault(a => a.username == postResponse.sender_username);

                        if (participantResponse != null)
                        {
                            //if (threadResponse.participants != null && threadResponse.participants.Count > 0)
                            //{
                            Message message = new Message()
                            {
                                Id = postResponse.id,
                                Text = HttpUtility.HtmlDecode(postResponse.content).Replace("<br />", "\n").Trim(' ', '\n', '\r'),
                                Title = HttpUtility.HtmlDecode(getMessagesResponse.thread.subject),
                                Author = HttpUtility.HtmlDecode(participantResponse.first_name + " " + participantResponse.last_name),
                                Date = DateTimeHelper.UnixTimeStampToDateTime(postResponse.created_ts),
                                //Direction = (postResponse.sender_username == UserName) ? MessageDirection.Outgoing : MessageDirection.Incoming,
                                IsRead = getMessagesResponse.thread.tray_item.read == 1,
                                Attachments = (postResponse.attachments != null) ? postResponse.attachments.Select(a => new Attachment() { FileSize = a.filesize, FileName = a.filename, Acl = a.acl, Url = a.url }).ToObservableCollection() : new ObservableCollection<Attachment>()
                            };
                            //message.Attachments.Add(new Attachment() { FileName = "image.png", Url = "http://dummyimage.com/100x100/456/F56" });
                            //message.Attachments.Add(new Attachment() { FileName = "image.png", Url = "http://dummyimage.com/100x100/856/856" });

                            if (!Conversation.Messages.Contains(message))
                            {
                                bool Added = false;

                                for (int i = 0; i < Conversation.Messages.Count; i++)
                                {
                                    if (message.CompareTo(Conversation.Messages[i]) < 0)
                                    {
                                        Conversation.Messages.Insert(i, message);
                                        Added = true;
                                        break;
                                    }
                                }

                                if (!Added)
                                {
                                    Conversation.Messages.Add(message);
                                }
                            }
                            else
                            {
                                Message message_ = Conversation.Messages.FirstOrDefault(m => m.Id == message.Id);

                                message_.Text = message.Text;
                                message_.Attachments = message.Attachments;
                            }
                        }
                        //}
                    }

                    //Conversation.Messages.Add(new Message() { Text = "sss", IsRead = false });
                    //Conversation.Messages.Last().Attachments.Add(new Attachment() { FileName = "image.png", Url = "http://dummyimage.com/100x100/856/856" });
                    //Conversation.Messages.Add(new Message() { Text = "sss", IsRead = false , Direction = MessageDirection.Incoming});
                    //Conversation.Messages.Last().Attachments.Add(new Attachment() { FileName = "image.png", Url = "http://dummyimage.com/100x100/856/856" });

                    Conversation conversation = App.ViewModel.Conversations.FirstOrDefault(c => c.Id == Conversation.Id);

                    if (conversation != null)
                    {
                        conversation.Messages = Conversation.Messages;

                        SaveRooms();
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                Conversation.IsMessagesLoading = false;

                if (ex.Message.Contains("Unauthorized"))
                {
                    LastSearch = null;
                    System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        App.RootFrame.Navigate(new Uri("/Views/OAuthPage.xaml", UriKind.RelativeOrAbsolute));
                    });
                }
                else
                {
                    //ErrorHelper.ShowErrorMessage(ex.InnerException.Message);
                }
            }

            return;
        }

        public async Task<bool> PostMessage(int threadId, string message)
        {
            if (!Api.IsAuthorized) return false;

            try
            {
                Conversation.IsMessagesLoading = true;

                PostMessageResponse postMessageResponse = await Api.PostMessage(User.FullName, threadId, message);

                Conversation.IsMessagesLoading = false;

                if (postMessageResponse != null)
                {
                    return true;
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                Conversation.IsMessagesLoading = false;

                if (ex.Message.Contains("Unauthorized"))
                {
                    LastSearch = null;
                    System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        App.RootFrame.Navigate(new Uri("/Views/OAuthPage.xaml", UriKind.RelativeOrAbsolute));
                    });

                    return false;
                }
                else
                {
                    //ErrorHelper.ShowErrorMessage(ex.InnerException.Message);

                    return false;
                }
            }
        }

        public async Task<bool> MessagesRead(int threadId)
        {
            if (!Api.IsAuthorized) return false;

            try
            {
                MessagesReadResponse messagesReadResponse = await Api.MessagesRead(User.FullName, threadId);

                if (messagesReadResponse != null && messagesReadResponse.IsSuccess)
                {
                    return true;
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Unauthorized"))
                {
                    LastSearch = null;
                    System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        App.RootFrame.Navigate(new Uri("/Views/OAuthPage.xaml", UriKind.RelativeOrAbsolute));
                    });

                    return false;
                }
                else
                {
                    //ErrorHelper.ShowErrorMessage(ex.InnerException.Message);

                    return false;
                }
            }
        }

        public void StartBackgroundAgent()
        {
            string periodicTaskName = "livetile";

            PeriodicTask periodicTask = ScheduledActionService.Find(periodicTaskName) as PeriodicTask;

            if (periodicTask != null)
            {
                RemoveBackgroundAgent();
            }

            periodicTask = new PeriodicTask(periodicTaskName);

            periodicTask.Description = "Live Tile update agent";

            try
            {
                ScheduledActionService.Add(periodicTask);

#if(DEBUG)
                ScheduledActionService.LaunchForTest(periodicTaskName, TimeSpan.FromSeconds(60));
#endif
            }
            catch (InvalidOperationException exception)
            {
                if (exception.Message.Contains("BNS Error: The action is disabled"))
                {
                    //MessageBox.Show("Background agents for this application have been disabled by the user.");
                }

                if (exception.Message.Contains("BNS Error: The maximum number of ScheduledActions of this type have already been added."))
                {
                    // No user action required. The system prompts the user when the hard limit of periodic tasks has been reached.
                }
            }
            catch (SchedulerServiceException)
            {
                // No user action required.
            }
        }

        public void RemoveBackgroundAgent()
        {
            string periodicTaskName = "livetile";

            try
            {
                ScheduledActionService.Remove(periodicTaskName);
            }
            catch (Exception)
            {
            }
        }
    }
}

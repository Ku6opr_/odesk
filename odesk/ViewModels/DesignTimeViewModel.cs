﻿using sharedcode.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace odesk.ViewModels
{
    public class DesignTimeViewModel : MainViewModel
    {
        public DesignTimeViewModel()
        {
            if (DesignerProperties.IsInDesignTool)
                {
                    RecentSearches = new ObservableCollection<string>();

                    LookingForJobs = new ObservableCollection<string>()
                    {
                        "Windows Phone",
                        "Windows Mobile"
                    };

                    Jobs = new ObservableCollection<Job>();
                    Jobs.Add(new Job() { SkillsList = new List<string>() { "html", "js" }, Title = "Permanent Punjabi translator (only From Punjab)", Description = "HI! we need a permanent Punjabi Translator for our website. We post different type of stuff and generally need Translation. PLEASE ONLY APPLY :- 1. We pay 1$ for 250 words ( Plus Bonus after 4-5 assignments) 2" });
                    Jobs.Add(new Job() { SkillsList = new List<string>() { "css", "windows-phone" }, Title = "Experience Web programmer needed for modifying wordpress template", Description = "Hi, I am looking for an experienced web programmer to help me modify the code of an existing wordpress template to better suit our needs" });

                    JobsFeed = new ObservableCollection<Job>();
                    JobsFeed.Add(new Job() { Client = "Sara Connor", Title = "Porting android app to windows phone 8", Description = "HI! we need a permanent Punjabi Translator for our website. We post different type of stuff and generally need Translation. PLEASE ONLY APPLY :- 1. We pay 1$ for 250 words ( Plus Bonus after 4-5 assignments) 2" });

                    Conversations = new ObservableCollection<Conversation>();
                    Conversations.Add(new Conversation()
                    {
                        Messages = new ObservableCollection<Message>()
                        {                    
                            new Message() { IsRead = false, Date = DateTime.Now, Author = "John Ugly", Title = "Windows Phone 8 Radio Player for live streaming XAML", Text = "Can you attend every requirements? an experienced web programmer to help me modify the code of an existing wordpress template to better suit our needs" },
                            new Message() { IsRead = true, Date = DateTime.Now.AddDays(-1), Author = "Marc Hogtie", Title = "Player for live streaming video", Text = "Can you experienced web programmer an existing wordpress template to better suit our needs" },
                            new Message() { IsRead = true, Date = DateTime.Now.AddDays(-4), Author = "Andrey Petrenko", Title = "looking for an experience", Text = "fferent type of stuff and generally need template to better suit our needs" }
                        }
                    });

                    Conversation = new Conversation()
                    {
                        Companion = "John",
                        Title = "Permanent Punjabi translator (only From Punjab)",
                        Messages = new ObservableCollection<Message>()
                    {
                        new Message() { Direction = MessageDirection.Incoming, IsRead = true, Date = DateTime.Now.AddDays(-4), Text = "Can you attend every requirements? an experienced web programmer to help me modify the code of an existing wordpress template to better suit our needs" },
                        new Message() { Direction = MessageDirection.Outgoing, IsRead = true, Date = DateTime.Now.AddDays(-2), Text = "Yes, I experienced web programmer an existing wordpress template to better suit our needs" },
                        new Message() { Direction = MessageDirection.Incoming, IsRead = false, Date = DateTime.Now.AddDays(-2), Text = "streaming video Translator" },
                        new Message() { Direction = MessageDirection.Outgoing, IsRead = false, Date = DateTime.Now.AddDays(-2), Text = "Requirements? Yes" },
                        new Message() { Direction = MessageDirection.Incoming, IsRead = false, Date = DateTime.Now.AddDays(-2), Text = "help me an existing wordpress template" },
                        new Message() { Direction = MessageDirection.Incoming, IsRead = false, Date = DateTime.Now.AddDays(-2), Text = "attend better suit" },
                        new Message() { Direction = MessageDirection.Outgoing, IsRead = false, Date = DateTime.Now.AddDays(-2), Text = "type of stuff and attend every requirements generally need" },
                        new Message() { Direction = MessageDirection.Outgoing, IsRead = false, Date = DateTime.Now.AddDays(-2), Text = "Requirements? Yes" },
                        new Message() { Direction = MessageDirection.Incoming, IsRead = false, Date = DateTime.Now.AddDays(-2), Text = "help me an existing wordpress template. xperienced web programmer an existing wordpress template to better t type of stuff and generally need Translation. PLEASE ONLY Plus Bonus after 4-5 " },
                        new Message() { Direction = MessageDirection.Incoming, IsRead = false, Date = DateTime.Now.AddDays(-2), Text = "attend better suit" },
                        new Message() { Direction = MessageDirection.Outgoing, IsRead = false, Date = DateTime.Now.AddDays(-1), Text = "type of stuff and attend every requirements generally need" },
                        new Message() { Direction = MessageDirection.Incoming, IsRead = false, Date = DateTime.Now.AddDays(-2), Text = "help me an existing wordpress template" },
                        new Message() { Direction = MessageDirection.Incoming, IsRead = false, Date = DateTime.Now.AddDays(-2), Text = "attend better suit" },
                        new Message() { Direction = MessageDirection.Outgoing, IsRead = false, Date = DateTime.Now.AddDays(-2), Text = "type of stuff and attend every requirements generally need" },
                        new Message() { Direction = MessageDirection.Outgoing, IsRead = false, Date = DateTime.Now.AddDays(-2), Text = "video Trans" },
                        new Message() { Direction = MessageDirection.Incoming, IsRead = false, Date = DateTime.Now.AddDays(-2), Text = "help me an existing wordpress template. xperienced web programmer an existing wordpress template to better t type of stuff and generally need Translation. PLEASE ONLY Plus Bonus after 4-5 " },
                        new Message() { Direction = MessageDirection.Incoming, IsRead = false, Date = DateTime.Now.AddDays(-2), Text = "attend better suit" },
                        new Message() { Direction = MessageDirection.Outgoing, IsRead = false, Date = DateTime.Now.AddDays(-1), Text = "type of stuff and attend every requirements generally need" },
                        new Message() { Direction = MessageDirection.Incoming, IsRead = false, Date = DateTime.Now.AddDays(-2), Text = "help me an existing wordpress template" },
                        new Message() { Direction = MessageDirection.Incoming, IsRead = false, Date = DateTime.Now.AddDays(-2), Text = "attend better suit" },
                        new Message() { Direction = MessageDirection.Outgoing, IsRead = false, Date = DateTime.Now.AddDays(-2), Text = "type of stuff and attend every requirements generally need" },
                        new Message() { Direction = MessageDirection.Outgoing, IsRead = false, Date = DateTime.Now.AddDays(-2), Text = "web programmer" },
                        new Message() { Direction = MessageDirection.Incoming, IsRead = false, Date = DateTime.Now.AddDays(-2), Text = "help me an existing wordpress template. xperienced web programmer an existing wordpress template to better t type of stuff and generally need Translation. PLEASE ONLY Plus Bonus after 4-5 " },
                        //new Message() { Direction = MessageDirection.Incoming, IsRead = false, Date = DateTime.Now.AddDays(-2), Text = "attend better suit" },
                        //new Message() { Direction = MessageDirection.Outgoing, IsRead = false, Date = DateTime.Now.AddDays(-1), Text = "type of stuff and attend every requirements generally need" },
                        //new Message() { Direction = MessageDirection.Outgoing, IsRead = false, Date = DateTime.Now.AddDays(-1), Text = "we need a permanent stuff" },
                        //new Message() { Direction = MessageDirection.Incoming, IsRead = false, Date = DateTime.Now.AddDays(-1), Text = "Good look" }
                    }
                    };

                    Job = new Job() { Client = "Mr. Been", Title = "Permanent Punjabi translator (only From Punjab)", Description = "HI! we need a permanent Punjabi Translator for our website. We post different type of stuff and generally need Translation. PLEASE ONLY APPLY :- 1. We pay 1$ for 250 words ( Plus Bonus after 4-5 assignments) Recommends potential products or services to management by collecting customer information and analyzing customer needs 2 am looking for an experienced web programmer to help me modify the code of an existing wordpress template to better suit our needs am looking for an experienced web programmer to help me modify Resolves product or service problems by clarifying the customer’s complaint; determining the cause of the problem; selecting and explaining the best solution to solve the problem; expediting correction or adjustment; following up to ensure resolution the code of an existing wordpress template to better suit our needs", SkillsList = new List<string>() { "html", "php" } };
                }
            }
    }
}

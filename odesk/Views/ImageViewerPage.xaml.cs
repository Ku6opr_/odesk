﻿using System;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using System.Windows.Media.Imaging;
using System.Net;

namespace odesk.Views
{
    public partial class ImageViewerPage : PhoneApplicationPage
    {
        public ImageViewerPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (NavigationContext.QueryString.ContainsKey("url"))
            {
                string url = HttpUtility.UrlDecode(NavigationContext.QueryString["url"]);

                AttachmentImage.Source = new BitmapImage(new Uri(url, UriKind.Absolute));
            }

            base.OnNavigatedTo(e);
        }
    }
}
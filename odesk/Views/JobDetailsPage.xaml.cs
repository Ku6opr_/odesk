﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System.Windows.Documents;
using odesk.Helpers;
using System.Windows.Media;

namespace odesk.Views
{
    public partial class JobDetailsPage : PhoneApplicationPage
    {
        public JobDetailsPage()
        {
            InitializeComponent();

            LogEntries.LogEntriesService.Log("job details page + " + Environment.OSVersion.Version);

            DataContext = App.ViewModel;
        }

        private void ShareButton_Click(object sender, System.EventArgs e)
        {
            ShareLinkTask shareLinkTask = new ShareLinkTask();

            shareLinkTask.Title = App.ViewModel.Job.Title;
            shareLinkTask.LinkUri = new Uri(App.ViewModel.Job.Url, UriKind.Absolute);
            shareLinkTask.Message = (App.ViewModel.Job.Description.Length < 100) ? App.ViewModel.Job.Description : App.ViewModel.Job.Description.Substring(0, 97) + "...";

            shareLinkTask.Show();
        }

        private void OpenButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            WebBrowserTask webBrowserTask = new WebBrowserTask();
            webBrowserTask.Uri = new Uri(App.ViewModel.Job.Url);
            webBrowserTask.Show();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using sharedcode.Models;

namespace odesk.Views
{
    public partial class SearchJobPage : PhoneApplicationPage
    {
        private bool dismissFocus = false;

        public SearchJobPage()
        {
            InitializeComponent();

            LogEntries.LogEntriesService.Log("search job page + " + Environment.OSVersion.Version);

            DataContext = App.ViewModel;

            App.ViewModel.IsSearchPerformed = false;

            SearchJobsText.Text = App.ViewModel.LastSearch ?? "";

            Dispatcher.BeginInvoke(() =>
            {
                SearchJobsText.Focus();
                SearchJobsText.SelectAll();
            });
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            base.OnNavigatedTo(e);
        }

        private void SearchJobText_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
        	if (e.Key == System.Windows.Input.Key.Enter && SearchJobsText.Text.Length > 2)
            {
                LogEntries.LogEntriesService.Log("search: " + SearchJobsText.Text);

                App.ViewModel.IsSearchPerformed = true;

                App.ViewModel.Jobs.Clear();

                App.ViewModel.SaveRecommendedJobs();

                App.ViewModel.Search(SearchJobsText.Text);

                this.Focus();

                //ApplicationBar.IsVisible = true;
            }
        }

        private void FollowButton_Click(object sender, System.EventArgs e)
        {
            (ApplicationBar.Buttons[0] as ApplicationBarIconButton).IsEnabled = false;
            App.ViewModel.LookingForJobs.Add(SearchJobsText.Text);
            //App.ViewModel.JobsFeed.Add(new odesk.Models.Job() { Title = "Permanent Punjabi translator (only From Punjab)", Description = "HI! we need a permanent Punjabi Translator for our website. We post different type of stuff and generally need Translation. PLEASE ONLY APPLY :- 1. We pay 1$ for 250 words ( Plus Bonus after 4-5 assignments) 2" });
        }

        private void SearchJobsList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (SearchJobsList.SelectedItem != null)
            {
                App.ViewModel.Job = SearchJobsList.SelectedItem as Job;

                NavigationService.Navigate(new Uri("/Views/JobDetailsPage.xaml", UriKind.RelativeOrAbsolute));

                SearchJobsList.SelectedItem = null;
            }
        }

        private void SearchJobsText_LostFocus(object sender, RoutedEventArgs e)
        {

        }

        private void SearchJobsText_GotFocus(object sender, RoutedEventArgs e)
        {

        }

        private void SearchJobsText_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
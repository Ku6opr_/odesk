﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using sharedcode.Server;
using sharedcode.Resources;
using sharedcode.Server.Responses;
using AsyncOAuth;
using System.Diagnostics;

namespace odesk.Views
{
    public partial class OAuthPage : PhoneApplicationPage
    {
        Stack<Uri> history = new Stack<Uri>();
        Uri current = null;
        string AuthorizeUrl;
        bool IsNeedRestart = false;

        string oAuthVerifier = String.Empty;

        public OAuthPage()
        {
            InitializeComponent();

            SystemTray.SetProgressIndicator(this, new ProgressIndicator() { IsVisible = true, Text = "Loading Upwork..." });

            webBrowser.Navigating += async(s, e) =>
            {
                LoadingBar.IsIndeterminate = true;
                LoadingBar.Visibility = Visibility.Visible;

                oAuthVerifier = Api.GetOAuthVerifier(e.Uri.OriginalString);

                if (!String.IsNullOrEmpty(oAuthVerifier))
                {
                    webBrowser.Visibility = System.Windows.Visibility.Collapsed;

                    AccessToken accessToken = await Api.GetAccessToken();

                    if (accessToken != null)
                    {
                        NavigationService.Navigate(new Uri("/Views/MainPage.xaml", UriKind.Relative));
                    }
                    else
                    {
                        SignInText.Text = "Authorization failed";
                        IsNeedRestart = true;
                    }
                }
            };

            webBrowser.NavigationFailed += (s, e) =>
            {
                if (String.IsNullOrEmpty(oAuthVerifier))
                {
                    SystemTray.ProgressIndicator.Text = "";
                    LoadingBar.IsIndeterminate = false;
                    LoadingBar.Visibility = Visibility.Collapsed;

                    MessageBox.Show("Something went wrong", "Upwork", MessageBoxButton.OK);
                }
            };

            webBrowser.Navigated += async (s, e) =>
            {
                Debug.WriteLine(e.Uri);

                SystemTray.ProgressIndicator.Text = "";
                LoadingBar.IsIndeterminate = false;
                LoadingBar.Visibility = Visibility.Collapsed;

                Uri previous = null;
                if (history.Count > 0)
                    previous = history.Peek();

                if (e.Uri == previous)
                {
                    history.Pop();
                }
                else
                {
                    if (current != null)
                    {
                        history.Push(current);
                    }
                }

                current = e.Uri;
                SystemTray.ProgressIndicator.IsIndeterminate = false; 
            };

            BackKeyPress += (s, e) =>
            {
                if (IsNeedRestart) return;

                if (history.Count > 0 && !String.Equals(current.ToString(), AuthorizeUrl, StringComparison.InvariantCultureIgnoreCase) && !String.Equals(current.ToString(), "https://www.upwork.com", StringComparison.InvariantCultureIgnoreCase))
                {
                    Uri destination = history.Peek();
                    webBrowser.Navigate(destination);
                    e.Cancel = true;
                }
            };
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            await webBrowser.ClearCookiesAsync();
            await webBrowser.ClearInternetCacheAsync();

            Authorize();

            while (NavigationService.CanGoBack)
            {
                NavigationService.RemoveBackEntry();
            }

            base.OnNavigatedTo(e);
        }

        public async void Authorize()
        {
            RequestToken requestToken = await Api.GetRequestToken();

            if (requestToken != null)
            {
                AuthorizeUrl = Api.GetAuthorizeUrl();

                webBrowser.Navigate(new Uri(AuthorizeUrl));
            }
        }
    }
}
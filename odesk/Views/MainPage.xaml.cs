﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using odesk.Resources;
using sharedcode.Models;
using sharedcode.Server;
using sharedcode.Server.Responses;
using odesk.Entities;
using sharedcode.Entities;
using sharedcode.Resources;
using Microsoft.Phone.Tasks;
using System.Threading.Tasks;

namespace odesk
{
    public partial class MainPage : PhoneApplicationPage
    {
        private bool IsMessageBoxOpen = false;

        public MainPage()
        {
            InitializeComponent();

            LogEntries.LogEntriesService.Log("main page + " + Environment.OSVersion.Version);

            DataContext = App.ViewModel;

            if (!Api.IsAuthorized)
            {
                Dispatcher.BeginInvoke(() =>
                {
                    NavigationService.Navigate(new Uri("/Views/OAuthPage.xaml", UriKind.RelativeOrAbsolute));
                });
            }
            else
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        App.ViewModel.LoadData();
                    });
                
                ShowRate();
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            while (NavigationService.CanGoBack)
            {
                NavigationService.RemoveBackEntry();
            }

            base.OnNavigatedTo(e);
        }

        private async void ShowRate()
        {
            if (!App.settings.Contains(Constants.LaunchCount))
            {
                App.settings[Constants.LaunchCount] = 0;
                App.settings.Save();
            }
            else
            {
                int launchCount = (int)App.settings[Constants.LaunchCount];
                App.settings[Constants.LaunchCount] = launchCount + 1;
                App.settings.Save();

                if (launchCount == 20)
                {
                    await Task.Delay(2000);

                    if (MessageBox.Show("Do you like to share your experience?", "Thanks for using oDesk", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                    {
                        MarketplaceReviewTask marketplaceReviewTask = new MarketplaceReviewTask();
                        marketplaceReviewTask.Show();
                    }
                }
            }
        }

        //private void SearchJobText_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    NavigationService.Navigate(new Uri("/Views/SearchJobPage.xaml", UriKind.RelativeOrAbsolute));
        //}

        private void MessagesList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (MessagesList.SelectedItem != null)
            {
                App.ViewModel.Conversation = (MessagesList.SelectedItem as Conversation);

                NavigationService.Navigate(new Uri("/Views/ConversationPage.xaml", UriKind.RelativeOrAbsolute));

                MessagesList.SelectedItem = null;
            }
        }

        private void JobsList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (JobsList.SelectedItem != null)
            {
                App.ViewModel.Job = (JobsList.SelectedItem as Job);

                NavigationService.Navigate(new Uri("/Views/JobDetailsPage.xaml", UriKind.RelativeOrAbsolute));

                JobsList.SelectedItem = null;
            }
        }

        private void LogOutButton_Click(object sender, System.EventArgs e)
        {
            Api.OAuthVerifier = null;
            Api.AccessToken = null;
            Api.AccessTokenSecret = null;
            App.ViewModel.LastSearch = null;
            App.ViewModel.User = null;
            Storage.DeleteFile(Constants.RecommendedJobs);
            Storage.DeleteFile(Constants.Rooms);

            App.ViewModel.Conversation = null;
            App.ViewModel.Conversations.Clear();
            App.ViewModel.Jobs.Clear();            
            App.ViewModel.JobsFeed.Clear();
            App.ViewModel.Messages.Clear();

            App.ViewModel.RemoveBackgroundAgent();

            NavigationService.Navigate(new Uri("/Views/OAuthPage.xaml", UriKind.Relative));
        }

        private void SearchButton_Click(object sender, System.EventArgs e)
        {
            if (panorama.SelectedIndex == 0)
            {
                NavigationService.Navigate(new Uri("/Views/SearchJobPage.xaml", UriKind.RelativeOrAbsolute));
            }
            else
            {
                NavigationService.Navigate(new Uri("/Views/SearchMessagePage.xaml", UriKind.RelativeOrAbsolute));
            }
        }

        private void ReviewButton_Click(object sender, System.EventArgs e)
        {
            MarketplaceReviewTask marketplaceReviewTask = new MarketplaceReviewTask();
            marketplaceReviewTask.Show();
        }

        private async void LockScreenButton_Click(object sender, System.EventArgs e)
        {
            await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-settings-lock:"));
        }

        private void MessagesList_ItemRealized(object sender, ItemRealizationEventArgs e)
        {
            Room item = (Room)e.Container.Content;

            if (item != null)
            {
                if (item.IsUpdate == false)
                {
                    App.ViewModel.LoadingRooms.Add(item.Id);

                    App.ViewModel.GetRoomInfo();
                }
            }
        }
    }
}
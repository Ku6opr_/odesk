﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using odesk.Helpers;
using sharedcode.Models;
using System.Windows.Documents;
using Microsoft.Phone.Tasks;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using sharedcode.Helpers;

namespace odesk.Views
{
    public partial class ConversationPage : PhoneApplicationPage
    {
        private bool IsNavigatingBack = false;
        private bool IsFirstLoading = true;

        private bool suppessScrollAnimation = false;

        public ConversationPage()
        {
            InitializeComponent();

            LogEntries.LogEntriesService.Log("conversation page + " + Environment.OSVersion.Version);

            DataContext = App.ViewModel;

            Initialize();

                //if (App.ViewModel.Conversation.Messages.Count > 0)
                //{
                //    MessagesList.ScrollTo(App.ViewModel.Conversation.Messages.Last());
                //}
                       

            Loaded += (s, e) =>
            {                
                //MessagesList.ScrollTo(App.ViewModel.Conversation.Messages.Last());
                //ContentPresenter contentPresenter = VisualTree.FindElementInVisualTree<ContentPresenter>(MessagesList);
                
                //MessagesList.GetValue(vie
                //if (App.ViewModel.Conversation.Messages.Count > 0)
                //{
                //    MessagesList.ScrollTo(App.ViewModel.Conversation.Messages.Last());
                //}

                if (IsFirstLoading)
                {
                    IsFirstLoading = false;

                    Dispatcher.BeginInvoke(async () =>
                    {
                        if (App.ViewModel.Conversation.Messages.Count > 0)
                        {
                            try
                            {
                                MessagesList.ScrollTo(App.ViewModel.Conversation.Messages.Last());
                            }
                            catch
                            { }
                            //MessagesList.ScrollPosition = 10000;
                            await Task.Delay(100);
                            MessagesList.ScrollPosition = 10000;
                        }
                        //await Task.Delay(25);
                        //MessagesList.ScrollPosition = 10000;
                        FadeIn.Begin();
                    });
                }
            };

            FadeOut.Completed += (s, e) =>
            {
                if (IsNavigatingBack)
                {
                    NavigationService.GoBack();
                }
            };

            BackKeyPress += (s, e) =>
            {
                if (NavigationService.CanGoBack)
                {
                    if (!IsNavigatingBack)
                    {
                        FadeOut.Begin();

                        IsNavigatingBack = true;
                    }
                    e.Cancel = true;
                }
            };
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (NavigationContext.QueryString.ContainsKey("id"))
            {
                int id = Int32.Parse(NavigationContext.QueryString["id"]);

                Dispatcher.BeginInvoke(() =>
                {
                    App.ViewModel.Conversation = App.ViewModel.Conversations.FirstOrDefault(c => c.Id == id);

                    if (App.ViewModel.Conversation == null)
                    {
                        App.ViewModel.Conversation = new Conversation() { Id = id };                         
                    }

                    Initialize();
                });

                NavigationContext.QueryString.Remove("id");
            }

            if (IsFirstLoading)
            {
                (HeaderGrid.RenderTransform as CompositeTransform).TranslateX = 0;
                CompanionButton.Opacity = 0.0;
                MessagesList.Opacity = 0.0;
                SendText.Opacity = 0.0;
            }
        }

        private void Initialize()
        {
            Dispatcher.BeginInvoke(async () =>
            {
                if (App.ViewModel.Conversation == null) return;

                int messagesCount = App.ViewModel.Conversation.Messages.Count;

                await App.ViewModel.GetMessages(App.ViewModel.Conversation.Id);

                Dispatcher.BeginInvoke(() =>
                {
                    List<Message> unread = App.ViewModel.Conversation.Messages.Where(m => !m.IsRead && m.Direction == MessageDirection.Incoming).ToList();

                    if (unread.Count > 0)
                    {
                        try
                        {                            
                            MessagesList.ScrollTo(unread.First());
                        }
                        catch
                        { }
                    }
                    else if (App.ViewModel.Conversation.Messages.Count > 0) ;// && messagesCount < App.ViewModel.Conversation.Messages.Count)
                    {
                        try
                        {
                            MessagesList.ScrollTo(App.ViewModel.Conversation.Messages.Last());
                        }
                        catch
                        { }
                    }
                    //MessagesList.ScrollPosition += 600;
                });

                //int unreadMessagesCount = 0;

                //foreach (Conversation conversation in App.ViewModel.Conversations)
                //{
                int unreadMessagesCount = App.ViewModel.Conversation.Messages.Count(m => !m.IsRead && m.Direction == MessageDirection.Incoming);
                //}

                if (unreadMessagesCount > 0)
                {
                    bool success = await App.ViewModel.MessagesRead(App.ViewModel.Conversation.Id);

                    if (success)
                    {
                        foreach (Conversation conversation in App.ViewModel.Conversations)
                        {
                            foreach (Message message in conversation.Messages)
                            {
                                message.IsRead = true;

                                App.ViewModel.SaveRooms();
                            }
                        }

                        TileHelper.UpdateTile(0);
                    }
                }
            });
        }

        private async void SendButton_Click(object sender, System.EventArgs e)
        {
            //MessagesList.ScrollPosition = DateTime.Now.Ticks;

            //Dispatcher.BeginInvoke(() =>
            //{
            //    Dispatcher.BeginInvoke(async () =>
            //    {
            (ApplicationBar.Buttons[0] as ApplicationBarIconButton).IsEnabled = false;

            bool success = await App.ViewModel.PostMessage(App.ViewModel.Conversation.Id, SendText.Text);

            (ApplicationBar.Buttons[0] as ApplicationBarIconButton).IsEnabled = true;

            if (success)
            {
                string title = App.ViewModel.Conversation.Messages.First().Title;

                App.ViewModel.Conversation.Messages.Add(new Message() { Title = title, Author = App.ViewModel.User.FullName, Text = SendText.Text, BriefText = SendText.Text, Date = DateTime.Now, IsRead = false, Direction = MessageDirection.Outgoing });

                //App.ViewModel.Conversations.Remove(App.ViewModel.Conversation);

                App.ViewModel.SortConversations();//.Insert(0, App.ViewModel.Conversation);

                //ScrollAnimationFrom.Value = MessagesList.ViewportTop;
                //ScrollAnimationTo.Value = MessagesList.ViewportTop + SendText.ActualHeight + 35;
                //ScrollAnimation.Begin();

                Dispatcher.BeginInvoke(() =>
                {
                    try
                    {
                        MessagesList.ScrollTo(App.ViewModel.Conversation.Messages.Last());
                    }
                    catch
                    { }

                    //suppessScrollAnimation = true;

                    SendText.Text = "";
                });
            }
            //    });
            //}); 
        }

        private void SendText_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            (ApplicationBar.Buttons[0] as ApplicationBarIconButton).IsEnabled = (SendText.Text.Length > 0);
        }

        private void SendText_SizeChanged(object sender, System.Windows.SizeChangedEventArgs e)
        {
            if (!suppessScrollAnimation)
            {
                //ScrollAnimationFrom.Value = MessagesList.ViewportTop;
                //ScrollAnimationTo.Value = MessagesList.ViewportTop + (e.NewSize.Height - e.PreviousSize.Height);
                //ScrollAnimation.Begin();
            }

            suppessScrollAnimation = false;
        }

        private void AttachmentLink_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            string url = ((sender as FrameworkElement).DataContext as Attachment).Url;

            new WebBrowserTask() { Uri = new Uri(url, UriKind.Absolute)}.Show();
        }

        private void ImagePanel_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string url = ((sender as FrameworkElement).DataContext as Attachment).Url;

            //if (url.StartsWith("https://odesk"))
            //{
                new WebBrowserTask() { Uri = new Uri(url, UriKind.Absolute) }.Show();
            //}
            //else
            //{
            //    NavigationService.Navigate(new Uri("/Views/ImageViewerPage.xaml?url=" + HttpUtility.UrlEncode(url), UriKind.Relative));
            //}
        }
    }
}
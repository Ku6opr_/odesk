﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using sharedcode.Models;
using System.Diagnostics;

namespace odesk.Views
{
    public partial class SearchMessagePage : PhoneApplicationPage
    {

        public SearchMessagePage()
        {
            InitializeComponent();

            LogEntries.LogEntriesService.Log("search messages page + " + Environment.OSVersion.Version);

            DataContext = App.ViewModel;

            App.ViewModel.IsSearchPerformed = false;

            Dispatcher.BeginInvoke(() =>
            {
                SearchMessageText.Focus();
                SearchMessageText.SelectAll();

                App.ViewModel.SearchMessages(String.Empty);
            });
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
        }

        private void SearchMessagesList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (SearchMessagesList.SelectedItem != null)
            {
                Conversation conversation = new Conversation() { Id = (SearchMessagesList.SelectedItem as Message).ConversationId };

                App.ViewModel.Conversation = App.ViewModel.Conversations.First(c => c.Equals(conversation));

                NavigationService.Navigate(new Uri("/Views/ConversationPage.xaml", UriKind.RelativeOrAbsolute));

                SearchMessagesList.SelectedItem = null;
            }
        }

        private void SearchMessageText_TextChanged(object sender, TextChangedEventArgs e)
        {
            App.ViewModel.IsSearchPerformed = true;

            App.ViewModel.SearchMessages(SearchMessageText.Text, SearchMessagesList.ActualWidth - 24);
        }

        private void SearchMessageText_GotFocus(object sender, RoutedEventArgs e)
        {
            SearchPlaceHolderTextBlock.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void SearchMessageText_LostFocus(object sender, RoutedEventArgs e)
        {
            if (SearchMessageText.Text.Count() > 0)
            {
                SearchPlaceHolderTextBlock.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                SearchPlaceHolderTextBlock.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void TextBlock_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (SearchMessageText.Text.Count() > 0)
            {
                double width = (sender as FrameworkElement).ActualWidth;

                Debug.WriteLine("ActualWidth: " + width);
            }
        }

        private void SearchMessagesList_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Debug.WriteLine("SearchMessagesList_SizeChanged");
            //if (SearchMessageText.Text.Count() > 0)
            //{
            //    Room room = (sender as FrameworkElement).DataContext as Room;

                
            //}
        }
    }
}
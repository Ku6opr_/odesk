﻿using sharedcode.Models;
using System.Windows;

namespace odesk.Entities
{
    public class MessageTemplateSelector : DataTemplateSelector
    {
        public DataTemplate Incoming
        {
            get;
            set;
        }

        public DataTemplate Outgoing
        {
            get;
            set;
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            switch ((item as Message).Direction)
            {
                case MessageDirection.Incoming:
                {
                    return Incoming;
                }
                case MessageDirection.Outgoing:
                {
                    return Outgoing;
                }
            }

            return base.SelectTemplate(item, container);
        }
    }
}

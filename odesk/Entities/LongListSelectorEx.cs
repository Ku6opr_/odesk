﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace odesk.Entities
{
    public class LongListSelectorEx : LongListSelector
    {
        private ViewportControl _viewportControl;
        
        // scroll postion is used to get scroll position 
        // and as we do not use ScrollTo also to set vertical top offset
        public double ScrollPosition
        {
            get { return (double)GetValue(ViewPortProperty); }
            set { SetValue(ViewPortProperty, value); }
        }

        public double ViewportTop { get; private set; }

        // this function is ovverided and is called first time 
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            _viewportControl = (ViewportControl)GetTemplateChild("ViewportControl");
            _viewportControl.ViewportChanged += OnViewportChanged;
            // This section is used for hiding scroll bar 
            // maybe you want to keep it I do not 
            //ScrollBar sb = ((FrameworkElement)VisualTreeHelper.GetChild(this, 0)).FindName("VerticalScrollBar") as ScrollBar;
            //sb.Width = 0;
        }
        // this event fires when position is changed
        private void OnViewportChanged(object sender, ViewportChangedEventArgs args)
        {
            ViewportControl viewport = (ViewportControl)sender;

            //ScrollPosition = _viewportControl.Viewport.Top;
            //System.Diagnostics.Debug.WriteLine(viewport.Viewport.Top);

            ViewportTop = _viewportControl.Viewport.Top;
            //System.Diagnostics.Debug.WriteLine(viewport.Viewport.Top);
        }
        // for XAML binding
        public static readonly DependencyProperty ViewPortProperty = DependencyProperty.Register(
            "ScrollPosition",
            typeof(double),
            typeof(LongListSelectorEx),
            new PropertyMetadata(0d, ViewPortChanged));
        private static void ViewPortChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            LongListSelectorEx mlls = (LongListSelectorEx)d;
            mlls._viewportControl.SetViewportOrigin(new Point(0, mlls.ScrollPosition));
            //mlls.ViewportTop = mlls._viewportControl.Viewport.Top;
            System.Diagnostics.Debug.WriteLine(mlls._viewportControl.Viewport.Top);
        }
    }
}

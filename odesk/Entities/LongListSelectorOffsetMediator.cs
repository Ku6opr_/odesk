﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace odesk.Entities
{
    public class LongListSelectorOffsetMediator : FrameworkElement
    {
        /// <summary>
        /// LongListSelector instance to forward Offset changes on to.
        /// </summary>
        public LongListSelectorEx LongListSelector
        {
            get { return (LongListSelectorEx)GetValue(LongListSelectorProperty); }
            set { SetValue(LongListSelectorProperty, value); }
        }
        public static readonly DependencyProperty LongListSelectorProperty =
            DependencyProperty.Register(
                "LongListSelector",
                typeof(LongListSelectorEx),
                typeof(LongListSelectorOffsetMediator),
                new PropertyMetadata(OnScrollViewerChanged));
        private static void OnScrollViewerChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var mediator = (LongListSelectorOffsetMediator)o;
            var longListSelector = (LongListSelectorEx)(e.NewValue);
            if (null != longListSelector)
            {
                longListSelector.ScrollPosition = mediator.VerticalOffset;
            }
        }

        /// <summary>
        /// VerticalOffset property to forward to the ScrollViewer.
        /// </summary>
        public double VerticalOffset
        {
            get { return (double)GetValue(VerticalOffsetProperty); }
            set { SetValue(VerticalOffsetProperty, value); }
        }
        public static readonly DependencyProperty VerticalOffsetProperty =
            DependencyProperty.Register(
                "VerticalOffset",
                typeof(double),
                typeof(LongListSelectorOffsetMediator),
                new PropertyMetadata(OnVerticalOffsetChanged));
        public static void OnVerticalOffsetChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var mediator = (LongListSelectorOffsetMediator)o;
            if (null != mediator.LongListSelector)
            {
                mediator.LongListSelector.ScrollPosition = (double)(e.NewValue);
            }
        }
    }
}

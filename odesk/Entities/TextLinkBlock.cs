﻿using odesk.Helpers;
using sharedcode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace odesk.Entities
{
    public class TextLinkBlock : RichTextBox
    {
        public static readonly DependencyProperty ContentProperty =
            DependencyProperty.Register("Content", typeof(string), typeof(TextLinkBlock), new PropertyMetadata("", new PropertyChangedCallback(OnContentPropertyChanged)));

        public string Content
        {
            get { return (string)GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }

        public Brush LinkForeground { get; set; }

        private static void OnContentPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as TextLinkBlock).Update((string)e.NewValue);            
        }

        public Run TrailRun { get; set; }

        private void Update(string text)
        {
            if (LinkForeground == null) LinkForeground = new SolidColorBrush(Colors.Blue);

            Blocks.Clear();
            Blocks.Add(LinksHelper.GenerateTextWithLinks(text, LinkForeground, (runs) =>
            {
                if (TrailRun != null)
                {
                    runs.Add(new Run() { Text = Message.TextFilling_, Foreground = TrailRun.Foreground, FontSize = TrailRun.FontSize });
                }
            }));
        }
    }
}

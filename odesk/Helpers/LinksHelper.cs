﻿using Microsoft.Phone.Tasks;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Documents;
using System.Windows.Media;

namespace odesk.Helpers
{
    public class LinksHelper
    {
        private static void AddTextWithLinks(List<Inline> runs, string message, Brush linkForeground)
        {
            message = RemoveEmailForwarding(message);

            Regex regex = new Regex(@"(\[\[.*\]\])|(http(s?):\S+?((\\r)|(\\n)|(\s)))");

            Match match = regex.Match(message);

            if (match.Success)
            {
                string before = message.Substring(0, match.Index);

                AddTextRuns(runs, before);

                string http = match.Value.TrimEnd('\r', '\n', ' ');

                string after = message.Substring(match.Index + http.Length);

                var link = new Hyperlink() { Foreground = linkForeground };

                if (http.Contains("|"))
                {
                    string[] parts = http.Substring(2, http.Length - 4).Split('|');

                    if (parts.Length == 2)
                    {
                        link.Inlines.Add(new Run() { Text = parts[1] });
                        link.Click += (sender, e) =>
                        {
                            var hyperLink = (sender as Hyperlink);
                            new WebBrowserTask() { Uri = new Uri(parts[0]) }.Show();
                        };
                    }
                }
                else
                {
                    link.Inlines.Add(new Run() { Text = http });
                    link.Click += (sender, e) =>
                    {
                        var hyperLink = (sender as Hyperlink);
                        new WebBrowserTask() { Uri = new Uri(http) }.Show();
                    };
                }

                runs.Add(link);

                if (after.Length > 0)
                {
                    AddTextWithLinks(runs, after, linkForeground);
                }
            }
            else
            {
                AddTextRuns(runs, message);
            }
        }

        private static void AddTextRuns(List<Inline> runs, string text)
        {
            string[] parts = text.Split('\n');

            for (int i = 0; i < parts.Length; i++)
            {
                runs.Add(new Run() { Text = parts[i] + ((i != parts.Length - 1) ? "\n" : "") });
            }
        }

        private static string RemoveEmailForwarding(string message)
        {
            List<string> lines = message.Split('\n').ToList<string>();

            bool forwardingFound = false;

            for (int i = lines.Count - 1; i >= 0; i--)
            {
                if (lines[i].StartsWith("> "))
                {
                    forwardingFound = true;

                    lines.RemoveAt(i);
                }
                else
                {
                    if (forwardingFound)
                    {
                        lines.RemoveAt(i);
                    }
                    break;
                }
            }

            if (forwardingFound)
            {
                message = String.Join("\n", lines).Trim(' ', '\n', '\r');
            }

            return message;
        }

        public static Paragraph GenerateTextWithLinks(string message, Brush linkForeground, Action<List<Inline>> callInEnd = null)
        {
            Paragraph paragraph = new Paragraph();

            List<Inline> runs = new List<Inline>();

            AddTextWithLinks(runs, message + " ", linkForeground);

            if (callInEnd != null)
            {
                callInEnd(runs);
            }

            foreach (var run in runs)
                paragraph.Inlines.Add(run);

            return paragraph;
        }
    }
}

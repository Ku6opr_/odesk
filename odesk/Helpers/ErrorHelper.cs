﻿using LogEntries;
using odesk.Resources;
using sharedcode.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace odesk.Helpers
{
    public class ErrorHelper
    {
        private static DateTime lastErrorTime = new DateTime(1);

        public static void ShowErrorMessage(string message)
        {
            LogEntriesService.Error(message);

            if (DateTime.Now.Subtract(lastErrorTime).TotalSeconds > 5)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    MessageBox.Show("No internet connection", Constants.ApplicationTitle, MessageBoxButton.OK);
                });
            }

            lastErrorTime = DateTime.Now;
        }
    }
}

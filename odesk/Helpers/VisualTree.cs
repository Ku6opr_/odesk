﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace odesk.Helpers
{
    class VisualTree
    {
        public static T FindElementInVisualTree<T>(DependencyObject parentElement) where T : DependencyObject
        {
            var count = VisualTreeHelper.GetChildrenCount(parentElement);
            if (count == 0)
                return null;

            for (int i = 0; i < count; i++)
            {
                var child = VisualTreeHelper.GetChild(parentElement, i);

                if (child != null && child is FrameworkElement && child is T)
                {
                    return (T)child;
                }
                else
                {
                    var result = FindElementInVisualTree<T>(child);
                    if (result != null)
                        return result;

                }
            }
            return null;
        }

        public static FrameworkElement SearchVisualTree(DependencyObject targetElement, object item, string elementName, int level = 0)
        {
            FrameworkElement res = null;
            var count = VisualTreeHelper.GetChildrenCount(targetElement);
            if (count == 0)
                return res;

            for (int i = 0; i < count; i++)
            {
                var child = VisualTreeHelper.GetChild(targetElement, i);
                //Debug.WriteLine(new string('-', level) + child.ToString() + " " + (child as FrameworkElement).Name);
                if (/*(child as FrameworkElement).DataContext == item &&*/ (child as FrameworkElement).Name == elementName)
                {
                    res = child as FrameworkElement;
                    return res;
                }
                else
                {
                    res = SearchVisualTree(child, item, elementName, level + 1);
                    if (res != null)
                        return res;
                }
            }
            return res;
        }
    }
}

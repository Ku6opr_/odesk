﻿using System;
using System.Windows.Data;

namespace odesk.Converters
{
    public class StringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string String = (string)value;
            string Format = (string)parameter;

            if (String == null) return "";

            switch (Format)
            {
                case "ToUpper":
                    return String.ToUpper();
                case "ToLower":
                    return String.ToLower();
            }

            return String.Format(Format, String);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Globalization;

namespace odesk.Converters
{
    public class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool visibility = (bool)value;

            if (parameter != null)
            {
                if (parameter.ToString().Equals("Inverse"))
                {
                    visibility = !visibility;
                }
            }

            return (visibility) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

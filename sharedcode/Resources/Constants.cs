﻿namespace sharedcode.Resources
{
    public class Constants
    {
        public const string ApplicationTitle = "oDesk";

        public const string LastSearch = "LastSearch";

        public const string MainViewModel = "MainViewModel";

        public const string Rooms = "Rooms";
        public const string Messages = "Messages";
        public const string RecommendedJobs = "RecommendedJobs";

        public const string LaunchCount = "LaunchCount";   
    }
}

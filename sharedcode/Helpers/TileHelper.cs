﻿using Microsoft.Phone.Shell;
using sharedcode.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace sharedcode.Helpers
{
    public class TileHelper
    {
        public static void UpdateTile(int count, string content1 = "", string content2 = "", string content3 = "")
        {
            try
            {
                ShellTile Tile = ShellTile.ActiveTiles.First();

                if (Tile != null)
                {
                    IconicTileData TileData = new IconicTileData()
                    {
                        Title = Constants.ApplicationTitle,
                        Count = count,
                        IconImage = new Uri("/Assets/Tiles/logo202.png", UriKind.Relative),
                        SmallIconImage = new Uri("/Assets/Tiles/logo110.png", UriKind.Relative),
                        BackgroundColor = Color.FromArgb(255, 0, 174, 236),
                        WideContent1 = content1,
                        WideContent2 = content2,
                        WideContent3 = content3
                    };

                    Tile.Update(TileData);
                }
            }
            catch
            { }
        }
    }
}

﻿using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Runtime.Serialization;

namespace sharedcode.Entities
{
    public class Storage
    {
        public static void SaveToFile(string path, object objForSave)
        {
            try
            {
                IsolatedStorageFile Store = IsolatedStorageFile.GetUserStoreForApplication();

                lock (Store)
                {
                    using (IsolatedStorageFileStream file = new IsolatedStorageFileStream(path, FileMode.Create, FileAccess.Write, Store))
                    {
                        Serialize(file, objForSave);

                        file.Close();
                    };
                }
            }
            catch { }
        }

        public static T LoadFromFile<T>(string path)
        {
            T obj = default(T);

            IsolatedStorageFile Store = null;

            try
            {
                Store = IsolatedStorageFile.GetUserStoreForApplication();

                if (!Store.FileExists(path)) return obj;
            
                lock (Store)
                {
                    using (IsolatedStorageFileStream file = new IsolatedStorageFileStream(path, FileMode.Open, FileAccess.Read, Store))
                    {
                        obj = (T)Deserialize(file, typeof(T));

                        file.Close();
                    };
                }
            }
            catch
            {
                try
                {
                    Store.DeleteFile(path);
                }
                catch { }
            }

            return obj;
        }

        public static void DeleteFile(string path)
        {
            try
            {
                IsolatedStorageFile Store = IsolatedStorageFile.GetUserStoreForApplication();

                if (Store.FileExists(path))
                {
                    Store.DeleteFile(path);
                }
            }
            catch { }
        }

        private static void Serialize(Stream streamObject, object objForSerialization)
        {
            if (objForSerialization == null || streamObject == null)
                return;

            DataContractSerializer ser = new DataContractSerializer(objForSerialization.GetType());
            ser.WriteObject(streamObject, objForSerialization);
        }

        private static object Deserialize(Stream streamObject, Type serializedObjectType)
        {
            if (serializedObjectType == null || streamObject == null)
                return null;

            DataContractSerializer ser = new DataContractSerializer(serializedObjectType);
            return ser.ReadObject(streamObject);
        }
    }
}


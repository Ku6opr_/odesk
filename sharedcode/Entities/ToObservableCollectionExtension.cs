﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace sharedcode.Entities
{
    public static class ToObservableCollectionExtension
    {
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> enumerable)
        {
            ObservableCollection<T> list = new ObservableCollection<T>();

            if (enumerable == null) return list;

            foreach (T item in enumerable)
            {
                list.Add(item);
            }

            return list;
        }
    }
}

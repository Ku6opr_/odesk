﻿using Newtonsoft.Json;

namespace sharedcode.Server.Requests
{
    public class PostMessageRequest
    {
        [JsonProperty("body")]
        public string body { get; set; }
    }
}

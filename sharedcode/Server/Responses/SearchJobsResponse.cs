﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using sharedcode.Models;

namespace sharedcode.Server.Responses
{
    public class SearchJobsResponse
    {
        [JsonProperty("jobs")]
        public List<JobResponse> Jobs { get; set; } 
    }

    public class JobResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("snippet")]
        public string Snippet { get; set; }
        [JsonProperty("skills")]
        public List<string> SkillsList { get; set; }
        [JsonProperty("category")]
        public string Category { get; set; }
        [JsonProperty("subcategory")]
        public string Subcategory { get; set; }
        [JsonProperty("job_type")]
        public string Job_type { get; set; }
        [JsonProperty("duration")]
        public string Duration { get; set; }
        [JsonProperty("workload")]
        public string Workload { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("date_created")]
        public string Date_created { get; set; }
        [JsonProperty("publish_time")]
        public string Publish_time { get; set; }
        [JsonProperty("client")]
        public Client Client { get; set; } 
        
    }

    public class ClientResponse
    {
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("feedback")]
        public string Feedback { get; set; }
        [JsonProperty("reviews_count")]
        public string Reviews_count { get; set; }
        [JsonProperty("jobs_posted")]
        public string Jobs_posted { get; set; }
        [JsonProperty("past_hires")]
        public string Past_hires { get; set; }
        [JsonProperty("member_since")]
        public string Member_since { get; set; }
        [JsonProperty("total_spent")]
        public string Total_spent { get; set; }
        [JsonProperty("payment_verification_status")]
        public string Payment_verification_status { get; set; }
    }
}

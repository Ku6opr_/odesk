﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.Server.Responses
{
    public class PostResponse
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("created_ts")]
        public int created_ts { get; set; }
        [JsonProperty("attachments")]
        public List<AttachmentResponse> attachments { get; set; }
        [JsonProperty("preview")]
        public string preview { get; set; }
        [JsonProperty("content")]
        public string content { get; set; }        
        [JsonProperty("sender_username")]
        public string sender_username { get; set; }
    }
}

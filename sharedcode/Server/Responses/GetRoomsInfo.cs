﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.Server.Responses
{
    public class GetRoomsInfo
    {
        public int server_time { get; set; }
        public RoomsAuthUserResponse auth_user { get; set; }
        public RoomInfoResponse room { get; set; }
    }

    public class RoomInfoStoryResponse
    {
        public string storyId { get; set; }
        public long created { get; set; }
        public long updated { get; set; }
        public string userId { get; set; }
        public string message { get; set; }
        public bool isSystemStory { get; set; }
    }

    public class RoomInfoStoriesResponse
    {
        public List<RoomInfoStoryResponse> stories { get; set; }
        public string cursor { get; set; }
        public int total { get; set; }
    }

    public class RoomInfoResponse
    {
        public string roomId { get; set; }
        public string roomName { get; set; }
        public string orgId { get; set; }
        public string topic { get; set; }
        public object recentTimestamp { get; set; }
        public bool isFavorite { get; set; }
        public object lastReadTimestamp { get; set; }
        public int roomType { get; set; }
        public RoomInfoStoriesResponse stories { get; set; }
        public object users { get; set; }
        public bool isReadOnly { get; set; }
        public bool isHidden { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace sharedcode.Server.Responses
{    
    public class ThreadResponse
    {
        [JsonProperty("last_post_preview")]
        public string last_post_preview { get; set; }
        [JsonProperty("last_post_id")]
        public int last_post_id { get; set; }
        [JsonProperty("item_id")]
        public int item_id { get; set; }
        [JsonProperty("created_ts")]
        public int created_ts { get; set; }
        [JsonProperty("participants")]
        public List<ParticipantResponse> participants { get; set; }
        [JsonProperty("last_accessed_ts")]
        public string last_accessed_ts { get; set; }
        [JsonProperty("read")]
        public int read { get; set; }
        [JsonProperty("context")]
        public string context { get; set; }
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("do_not_reply")]
        public int do_not_reply { get; set; }
        [JsonProperty("subject")]
        public string subject { get; set; }
        [JsonProperty("deleted")]
        public int deleted { get; set; }
        [JsonProperty("last_post_ts")]
        public int last_post_ts { get; set; }
        [JsonProperty("last_post_attachments")]
        public List<AttachmentResponse> last_post_attachments { get; set; }
        [JsonProperty("last_post_sender")]
        public string last_post_sender { get; set; }
        [JsonProperty("last_accessed_post_id")]
        public int? last_accessed_post_id { get; set; }
        [JsonProperty("sender")]
        public string sender { get; set; }
        [JsonProperty("type")]
        public string type { get; set; }
        [JsonProperty("posts_count")]
        public int posts_count { get; set; }
        [JsonProperty("thread_api")]
        public string thread_api { get; set; }
        [JsonProperty("thread_url")]
        public string thread_url { get; set; }

        [JsonProperty("tray_item")]
        public TrayItemResponse tray_item { get; set; }
        [JsonProperty("posts")]
        public List<PostResponse> posts { get; set; }

        [JsonProperty("trays")]
        public List<UserInfo> Trays { get; set; }
    }
}

﻿using Newtonsoft.Json;

namespace sharedcode.Server.Responses
{
    public class ParticipantResponse
    {
        [JsonProperty("ciphertext")]
        public string ciphertext { get; set; }
        [JsonProperty("has_muted")]
        public int has_muted { get; set; }
        [JsonProperty("last_post_id")]
        public int? last_post_id { get; set; }
        [JsonProperty("last_post_ts")]
        public int? last_post_ts { get; set; }
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("is_bcc")]
        public int is_bcc { get; set; }
        [JsonProperty("first_name")]
        public string first_name { get; set; }
        [JsonProperty("username")]
        public string username { get; set; }
        [JsonProperty("last_name")]
        public string last_name { get; set; }
    }
}

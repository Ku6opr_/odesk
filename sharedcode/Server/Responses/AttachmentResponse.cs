﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.Server.Responses
{
    public class AttachmentResponse
    {
        [JsonProperty("filesize")]
        public int filesize { get; set; }
        [JsonProperty("filename")]
        public string filename { get; set; }
        [JsonProperty("acl")]
        public string acl { get; set; }
        [JsonProperty("url")]
        public string url { get; set; } 
    }
}

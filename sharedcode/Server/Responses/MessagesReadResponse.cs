﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace sharedcode.Server.Responses
{
    public class MessagesReadResponse
    {
        [JsonProperty("message")]
        public string message { get; set; }

        public bool IsSuccess
        {
            get
            {
                return message == "OK";
            }
        }
    }
}

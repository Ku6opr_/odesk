﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.Server.Responses
{
    public class GetUserResponse
    {
        [JsonProperty("auth_user")]
        public AuthUserResponse AuthUser { get; set; }

        [JsonProperty("info")]
        public UserInfo Info { get; set; }
    }

    public class AuthUserResponse
    {
        [JsonProperty("first_name")]
        public string first_name { get; set; }
        [JsonProperty("last_name")]
        public string last_name { get; set; }
        [JsonProperty("timezone")]
        public string timezone { get; set; }
        [JsonProperty("timezone_offset")]
        public string timezone_offset { get; set; }
    }

    public class UserInfo
    {
        [JsonProperty("portrait_100_img")]
        public string portrait_100_img { get; set; }
        [JsonProperty("company_url")]
        public string company_url { get; set; }
        [JsonProperty("portrait_32_img")]
        public string portrait_32_img { get; set; }
        [JsonProperty("portrait_50_img")]
        public string portrait_50_img { get; set; }
        [JsonProperty("has_agency")]
        public string has_agency { get; set; }
        [JsonProperty("ref")]
        public string @ref { get; set; }
        [JsonProperty("capacity")]
        public UserCapacity capacity { get; set; }
        [JsonProperty("location")]
        public UserLocation location { get; set; }
        [JsonProperty("profile_url")]
        public string profile_url { get; set; }
    }

    public class UserCapacity
    {
        [JsonProperty("provider")]
        public string provider { get; set; }
        [JsonProperty("buyer")]
        public string buyer { get; set; }
        [JsonProperty("affiliate_manager")]
        public string affiliate_manager { get; set; }
    }

    public class UserLocation
    {
        [JsonProperty("city")]
        public string city { get; set; }
        [JsonProperty("state")]
        public string state { get; set; }
        [JsonProperty("country")]
        public string country { get; set; }
    }
}

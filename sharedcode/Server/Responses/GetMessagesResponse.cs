﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace sharedcode.Server.Responses
{
    public class GetMessagesResponse
    {
        [JsonProperty("thread")]
        public ThreadResponse thread{ get; set; }         
    }
}

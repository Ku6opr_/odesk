﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.Server.Responses
{
    public class GetUserReference
    {
        public string server_time { get; set; }
        public AuthUserReference auth_user { get; set; }
        public UserReference user { get; set; }
    }

    public class AuthUserReference
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string timezone { get; set; }
        public string timezone_offset { get; set; }
    }

    public class UserReference
    {
        public string reference { get; set; }
        public string timezone { get; set; }
        public string is_provider { get; set; }
        public string first_name { get; set; }
        public string public_url { get; set; }
        public string last_name { get; set; }
        public string timezone_offset { get; set; }
        public string email { get; set; }
        public string status { get; set; }
        public string id { get; set; }
        public string profile_key { get; set; }
    }
}

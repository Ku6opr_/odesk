﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.Server.Responses
{
    public class GetRoomsResponse
    {
        public int server_time { get; set; }
        public RoomsAuthUserResponse auth_user { get; set; }
        public List<RoomResponse> rooms { get; set; }
    }

    public class RoomsAuthUserResponse
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string timezone { get; set; }
        public string timezone_offset { get; set; }
    }

    public class RoomLatestStory
    {
        public object created { get; set; }
        public object updated { get; set; }
    }

    public class RoomResponse
    {
        public string roomId { get; set; }
        public string roomName { get; set; }
        public string orgId { get; set; }
        public string topic { get; set; }
        public int numUnread { get; set; }
        public RoomLatestStory latestStory { get; set; }
        public bool isFavorite { get; set; }
        public int numUnreadMentions { get; set; }
        public int numUsers { get; set; }
        public int roomType { get; set; }
        public object users { get; set; }
        public string appId { get; set; }
    }
}

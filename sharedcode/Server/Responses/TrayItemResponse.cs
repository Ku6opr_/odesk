﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.Server.Responses
{
    public class TrayItemResponse
    {
        [JsonProperty("starred")]
        public int starred { get; set; }
        [JsonProperty("deleted")]
        public int deleted { get; set; }
        [JsonProperty("last_accessed_ts")]
        public string last_accessed_ts { get; set; }
        [JsonProperty("last_accessed_post_id")]
        public string last_accessed_post_id { get; set; }
        [JsonProperty("read")]
        public int read { get; set; }
        
    }
}

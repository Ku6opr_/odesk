﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace sharedcode.Server.Responses
{
    public class GetTraysContentResponse
    {
        [JsonProperty("trays")]
        public List<UserInfo> Trays { get; set; }

        [JsonProperty("current_tray")]
        public CurrentTrayResponse CurrentTray { get; set; }        
    }

    public class ListerResponse
    {
        [JsonProperty("total_items")]
        public int total_items { get; set; }
    }

    public class CurrentTrayResponse
    {
        [JsonProperty("lister")]
        public ListerResponse Lister { get; set; }
        [JsonProperty("unread")]
        public int unread { get; set; }
        [JsonProperty("type")]
        public string type { get; set; }
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("threads")]
        public List<ThreadResponse> threads { get; set; }         
    }
}

﻿using AsyncOAuth;
using Newtonsoft.Json;
using sharedcode.Server.Requests;
using sharedcode.Server.Responses;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace sharedcode.Server
{
    public class Api
    {
        //var client = OAuthUtility.CreateOAuthClient("consumerKey", "consumerSecret", new AccessToken("accessToken", "accessTokenSecret"));

        //// Get
        //var json = await client.GetStringAsync("http://api.twitter.com/1.1/statuses/home_timeline.json?count=" + count + "&page=" + page);

        //// Post
        //var content = new FormUrlEncodedContent(new[] { new KeyValuePair<string, string>("status", status) });
        //var response = await client.PostAsync("http://api.twitter.com/1.1/statuses/update.json", content);
        //var json = await response.Content.ReadAsStringAsync();

        //// Multi Post
        //var content = new MultipartFormDataContent();
        //content.Add(new StringContent(status), "\"status\"");
        //content.Add(new ByteArrayContent(media), "media[]", "\"" + fileName + "\"");

        //var response = await client.PostAsync("https://upload.twitter.com/1/statuses/update_with_media.json", content);
        //var json = await response.Content.ReadAsStringAsync();

        private readonly static IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

        private const string requestTokenUrl = "https://www.upwork.com/api/auth/v1/oauth/token/request?oauth_callback=upwork.com";
        private const string authorizeUrl = "https://www.upwork.com/services/api/auth";
        private const string accessTokenUrl = "https://www.upwork.com/api/auth/v1/oauth/token/access";

        public const string key = "a8026dab00751271bce77717588da63c";
        public const string secret = "12472a968c3fabb5";
        //public const string key = "91cf2a3879cad9c02e6245684cf792ad";
        //public const string secret = "2ea5f4b4aaab63af";
        private const string server = "https://www.upwork.com";
        private const string contentType = "application/json";
        private static HttpMethod get = HttpMethod.Get;
        private static HttpMethod post = HttpMethod.Post;
        private static HttpMethod put = HttpMethod.Put;

        private static RequestToken requestToken;

        private static string oAuthVerifier;
        public static string OAuthVerifier
        {
            get
            {
                if (oAuthVerifier != null)
                {
                    return oAuthVerifier;
                }

                if (settings.Contains("OAuthVerifier"))
                {
                    oAuthVerifier = (string) settings["OAuthVerifier"];

                    return oAuthVerifier;
                }

                return null;
            }
            set
            {
                oAuthVerifier = value;
                settings["OAuthVerifier"] = oAuthVerifier;
                settings.Save();
            }
        }

        private static string accessToken;
        public static string AccessToken
        {
            get
            {
                if (accessToken != null)
                {
                    return accessToken;
                }

                if (settings.Contains("AccessToken"))
                {
                    accessToken = (string)settings["AccessToken"];

                    return accessToken;
                }

                return null;
            }
            set
            {
                accessToken = value;
                settings["AccessToken"] = accessToken;
                settings.Save();
            }
        }

        private static string accessTokenSecret;
        public static string AccessTokenSecret
        {
            get
            {
                if (accessTokenSecret != null)
                {
                    return accessTokenSecret;
                }

                if (settings.Contains("AccessTokenSecret"))
                {
                    accessTokenSecret = (string)settings["AccessTokenSecret"];

                    return accessTokenSecret;
                }

                return null;
            }
            set
            {
                accessTokenSecret = value;
                settings["AccessTokenSecret"] = accessTokenSecret;
                settings.Save();
            }
        }

        public static bool IsAuthorized
        {
            get
            {
                return OAuthVerifier != null && AccessToken != null && AccessTokenSecret != null;
            }
        }

        private static OAuthAuthorizer authorizer;

        static Api()
        {
            OAuthUtility.ComputeHash = (key, buffer) => { using (var hmac = new HMACSHA1(key)) { return hmac.ComputeHash(buffer); } };
        }

        public static async Task<RequestToken> GetRequestToken()
        {
            authorizer = new OAuthAuthorizer(key, secret);

            try
            {
                TokenResponse<RequestToken> tokenResponse = await authorizer.GetRequestToken(requestTokenUrl);

                requestToken = tokenResponse.Token;

                return tokenResponse.Token;
            }
            catch { }

            return null;
        }

        public static string GetAuthorizeUrl()
        {
            return authorizer.BuildAuthorizeUrl(authorizeUrl, requestToken);
        }

        public static string GetOAuthVerifier(string url)
        {
            if (url.Contains("oauth_verifier"))
            {
                int index = url.IndexOf("oauth_verifier=") + "oauth_verifier=".Length;

                OAuthVerifier =  url.Substring(index, url.Length - index);

                return OAuthVerifier;
            }
            else
            {
                return null;
            }
        }

        public static async Task<AccessToken> GetAccessToken()
        {
            try
            {
                TokenResponse<AccessToken> tokenResponse = await authorizer.GetAccessToken(accessTokenUrl, requestToken, OAuthVerifier);

                if (tokenResponse != null && tokenResponse.Token != null)
                {
                    AccessToken = tokenResponse.Token.Key;
                    AccessTokenSecret = tokenResponse.Token.Secret;

                    return tokenResponse.Token;
                }
            }
            catch { }

            return null;
        }

        public static async Task<SearchJobsResponse> SearchJobs(string searchstring)
        {
            return await Call<SearchJobsResponse>(String.Format("{0}/profiles/v2/search/jobs.json?q={1}&paging=0;100", server, searchstring), get, "");
        }

        public static async Task<GetUserResponse> GetUserInfo()
        {
            return await Call<GetUserResponse>(String.Format("{0}/api/auth/v1/info.json", server), get, "");
        }

        public static async Task<GetUserReference> GetUserReference()
        {
            return await Call<GetUserReference>(String.Format("{0}/api/hr/v2/users/me.json", server), get, "");
        }

        public static async Task<GetRoomsResponse> GetRooms(string userId)
        {
            return await Call<GetRoomsResponse>(String.Format("{0}/api/messages/v3/{1}/rooms.json?time={2}", server, userId, DateTime.Now.Ticks), get, "");
        }

        public static async Task<GetRoomsInfo> GetRoomInformation(string userId, string roomId)
        {
            Debug.WriteLine(DateTime.Now.Second);

            return await Call<GetRoomsInfo>(String.Format("{0}/api/messages/v3/{1}/rooms/{2}.json?returnStories=true", server, userId, roomId), get, "");
        }
        public static async Task<GetRoomsResponse> GetJobs(string userId)
        {
            return await Call<GetRoomsResponse>(String.Format("{0}/api/hr/v2/jobs.json", server), get, "");
        }
        public static async Task<GetUserReference> GetListCompanies()
        {
            return await Call<GetUserReference>(String.Format("{0}/api/hr/v2/companies.json", server), get, "");
        }

        public static async Task<GetMessagesResponse> GetMessages(string username, int threadId)
        {
            return await Call<GetMessagesResponse>(String.Format("{0}/mc/v1/threads/{1}/{2}.json", server, username, threadId), get, "");
        }

        public static async Task<PostMessageResponse> PostMessage(string username, int threadId, string message)
        {
            return await Call<PostMessageResponse>(String.Format("{0}/mc/v1/threads/{1}/{2}.json", server, username, threadId), post, JsonConvert.SerializeObject(new PostMessageRequest() { body = message }));
        }

        public static async Task<MessagesReadResponse> MessagesRead(string username, int threadId)
        {
            return await Call<MessagesReadResponse>(String.Format("{0}/mc/v1/threads/{1}/{2}.json", server, username, threadId), put, "{\"read\": \"true\"}");
        }

        private static Task<T> Call<T>(string address, HttpMethod method, string data)
        {
            return Task.Run<T>(async () =>
            {
                TaskCompletionSource<T> tcs = new TaskCompletionSource<T>();

                try
                {
                    string responseJson = await RequestAsync(address, method, contentType, /*JsonConvert.SerializeObject(*/data/*)*/);

                    Debug.WriteLine(responseJson);

                    T response = JsonConvert.DeserializeObject<T>(responseJson);

                    tcs.SetResult(response);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("Unauthorized"))
                    {
                        AccessToken = null;
                        AccessTokenSecret = null;
                        OAuthVerifier = null;
                    }

                    tcs.SetException(ex);
                }
                
                return await tcs.Task;
            });
        }

        private static async Task<string> RequestAsync(string address, HttpMethod method, string contentType, string data)
        {
            var authParams = OAuthUtility.BuildBasicParameters(key, secret, address, method, new AccessToken(AccessToken, AccessTokenSecret));

            string auth = "OAuth " + String.Join(",", authParams.Select(k => k.Key + "=\"" + k.Value + "\""));

            Debug.WriteLine(auth);

            return await RequestOAuth(address, method.ToString(), data, auth);
        }

        private static async Task<string> RequestOAuth(string address, string method, string data, string auth)
        {
            Debug.WriteLine(address);

            TaskCompletionSource<string> taskCompletionSource = new TaskCompletionSource<string>();

            HttpWebRequest request = HttpWebRequest.CreateHttp(address);
            request.Method = method;
            request.Headers["Authorization"] = auth;
            if (method == post.ToString() || method == put.ToString())
            {
                request.ContentType = contentType;

                Debug.WriteLine(data);

                var taskGetStream = Task<Stream>.Factory.FromAsync(request.BeginGetRequestStream, request.EndGetRequestStream, null);
                taskGetStream.Wait();
                using (var stream = taskGetStream.Result)
                {
                    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(data);
                    stream.Write(bytes, 0, bytes.Length);
                }
            }

            var taskGetResponse = Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, null);

            taskGetResponse.Wait();

            using (var response = taskGetResponse.Result)
            {
                using (var stream = response.GetResponseStream())
                {
                    var reader = new StreamReader(stream);
                    taskCompletionSource.SetResult(reader.ReadToEnd());
                }
            }

            return await taskCompletionSource.Task;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.Models
{
    [DataContractAttribute]
    public class Attachment : BaseModel
    {
        [DataMemberAttribute]
        public int FileSize { get; set; }
        [DataMemberAttribute]
        public string FileName { get; set; }
        [DataMemberAttribute]
        public string Acl { get; set; }
        [DataMemberAttribute]
        public string Url { get; set; }

        public bool IsImage
        {
            get
            {
                return FileName.EndsWith(".png") || FileName.EndsWith(".jpg") || FileName.EndsWith(".jpeg") || FileName.EndsWith(".bmp");
            }
        }
    }
}

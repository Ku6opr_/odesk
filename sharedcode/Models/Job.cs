﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.Models
{
    [DataContractAttribute]
    public class Job : BaseModel
    {
        [DataMemberAttribute]
        public string Id { get; set; }

        [DataMemberAttribute]
        public string ClientId { get; set; }

        [DataMemberAttribute]
        public string Client { get; set; }

        [DataMemberAttribute]
        public string Title { get; set; }

        [DataMemberAttribute]
        public string Description { get; set; }

        [DataMemberAttribute]
        public string BriefDescription
        {
            get
            {
                return Description.Replace("\r\n", " ");
            }
            set
            { }
        }

        [DataMemberAttribute]
        public List<string> SkillsList { get; set; }

        [DataMemberAttribute]
        public string Url { get; set; }

        [DataMemberAttribute]
        public bool IsSeen { get; set; }

        [DataMemberAttribute]
        public DateTime Date { get; set; }

        public static Job Create(string id, string clientId, string client, string title, string description, List<string> skills, string url, string date, bool isSeen = true)
        {
            return new Job() { Id = id, ClientId = clientId, Client = client, Title = title, Description = description, SkillsList = skills, Url = url, IsSeen = isSeen, Date = DateTime.Parse(date, CultureInfo.InvariantCulture).ToLocalTime() };
        }

        public override bool Equals(object obj)
        {
            if (obj as Job == null) return false;

            return (obj as Job).Id == Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public string Skills
        {
            get
            {
                return String.Join(", ", SkillsList);
            }
        }
    }
}

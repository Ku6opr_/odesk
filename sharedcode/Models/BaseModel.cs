﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows;

namespace sharedcode.Models
{
    [DataContractAttribute]
    public class BaseModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    if (PropertyChanged != null)
                        PropertyChanged(this, new PropertyChangedEventArgs(info));
                });
            }
        }
    }
}

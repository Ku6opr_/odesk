﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.Models
{
    [DataContractAttribute]
    public class Client : BaseModel
    {
        [DataMemberAttribute]
        public string Country { get; set; }

        [DataMemberAttribute]
        public string City { get; set; }

        [DataMemberAttribute]
        public string Feedback { get; set; }

        [DataMemberAttribute]
        public string Reviews_count { get; set; }

        [DataMemberAttribute]
        public string Jobs_posted { get; set; }

        [DataMemberAttribute]
        public string Past_hires { get; set; }

        [DataMemberAttribute]
        public string Member_since { get; set; }

        [DataMemberAttribute]
        public string Total_spent { get; set; }

        [DataMemberAttribute]
        public string Payment_verification_status { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.Models
{
    public class User
    {
        public string First_name { get; set; }
        public string Last_name { get; set; }
        public string ImageUrl { get; set; }
        public string Id { get; set; }
        public string Is_provider { get; set; }
        public string Public_url { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public string UpworkName { get; set; }
        public string Profile_key { get; set; }

        public string FullName
        {
            get
            {
                return First_name + " " + Last_name;
            }
        }
    }
}

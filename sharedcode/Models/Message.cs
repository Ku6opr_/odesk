﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.Models
{
    [DataContractAttribute]
    public class Message : BaseModel, IComparable<Message>
    {
        [DataMemberAttribute]
        public int Id { get; set; }

        [DataMemberAttribute]
        public string Author { get; set; }

        [DataMemberAttribute]
        public string Title { get; set; }

        private string text;
        [DataMemberAttribute]
        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                if (text != value)
                {
                    text = value;
                    NotifyPropertyChanged("Text");
                }
            }
        }

        [DataMemberAttribute]
        public string BriefText
        {
            get
            {
                return Text.Replace("\n", " ");
            }
            set
            { }
        }

        /// <summary>
        /// Используется для сдвига даты вниз на следующую строку, если она не влазит по ширине вместе с текстом
        /// </summary>
        public string TextFilling
        {
            get
            {
                return (IsAttachmentExists) ? "" : "_______________________";
            }
        }

        public static string TextFilling_
        {
            get
            {
                return "_______________________";
            }
        }

        private ObservableCollection<Attachment> attachments;
        [DataMemberAttribute]
        public ObservableCollection<Attachment> Attachments
        {
            get
            {
                return attachments;
            }
            set
            {
                if (attachments != value)
                {
                    attachments = value;
                    NotifyPropertyChanged("Attachments");
                    NotifyPropertyChanged("IsAttachmentExists");
                    NotifyPropertyChanged("TextFilling");                    

                    if (Attachments != null)
                    {
                        Attachments.CollectionChanged += (s, e) =>
                        {
                            NotifyPropertyChanged("IsAttachmentExists");
                            NotifyPropertyChanged("TextFilling");   
                        };
                    }
                }
            }
        }

        public bool IsAttachmentExists
        {
            get
            {
                return Attachments != null && Attachments.Count > 0;
            }
        }

        [DataMemberAttribute]
        public DateTime Date { get; set; }

        public string ShortDate
        {
            get
            {
                return Date.ToString("dd.MM");
            }
        }

        public string LongDate
        {
            get
            {
                return Date.ToString("dd.MM HH:mm");
            }
        }

        [DataMemberAttribute]
        public bool IsRead { get; set; }

        [DataMemberAttribute]
        public MessageDirection Direction { get; set; }

        [DataMemberAttribute]
        public int ConversationId { get; set; }

        public Message()
        {
            Attachments = new ObservableCollection<Attachment>();
        }

        public override bool Equals(object obj)
        {
            if (obj as Message == null) return false;

            return (obj as Message).Id == Id;
        }

        public override int GetHashCode()
        {
            return Id;
        }

        public override string ToString()
        {
            return Text;
        }

        public int CompareTo(Message other)
        {
            return Date.CompareTo(other.Date);
        }
    }

    public enum MessageDirection
    {
        Outgoing,
        Incoming
    }
}

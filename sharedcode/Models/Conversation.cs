﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.Models
{
    [DataContractAttribute]
    public class Conversation : BaseModel
    {
        [DataMemberAttribute]
        public int Id { get; set; }

        [DataMemberAttribute]
        public string Companion { get; set; }

        [DataMemberAttribute]
        public string Title { get; set; }

        private ObservableCollection<Message> messages = new ObservableCollection<Message>();
        [DataMemberAttribute]
        public ObservableCollection<Message> Messages
        {
            get
            {
                return messages;
            }
            set
            {
                if (messages != value)
                {
                    messages = value;
                    NotifyPropertyChanged("Messages");
                    NotifyPropertyChanged("IsMessagesExists");

                    if (Messages != null)
                    {
                        Messages.CollectionChanged += (s, e) =>
                        {
                            NotifyPropertyChanged("IsMessagesExists");
                        };
                    }
                }
            }
        }

        private bool isMessagesLoading = false;
        [DataMemberAttribute]
        public bool IsMessagesLoading
        {
            get
            {
                return isMessagesLoading;
            }
            set
            {
                if (isMessagesLoading != value)
                {
                    isMessagesLoading = value;
                    NotifyPropertyChanged("IsMessagesLoading");
                    NotifyPropertyChanged("IsMessagesExists");
                }
            }
        }

        public bool IsMessagesExists
        {
            get
            {
                return ((Messages == null || Messages.Count == 0) && !IsMessagesLoading) ? false : true;
            }
        }

        public Message PreviewMessage
        {
            get
            {
                if (Messages != null && Messages.Count > 0)
                {
                    return Messages.Last();
                }

                return null;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj as Conversation == null) return false;

            return (obj as Conversation).Id == Id;
        }

        public override int GetHashCode()
        {
            return Id;
        }
    }
}

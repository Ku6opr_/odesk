﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.Models
{
    public class Room : BaseModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string OrgId { get; set; }
        public string Topic { get; set; }
        public object RecentTimestamp { get; set; }
        public bool IsFavorite { get; set; }
        public object LastReadTimestamp { get; set; }
        public int RoomType { get; set; }       
        public object Users { get; set; }
        public bool IsReadOnly { get; set; }
        public bool IsHidden { get; set; }
        public DateTime LastUpdated { get; set; }
        public bool IsUpdate { get; set; }

        private string firstTitle;
        public string FirstTitle
        {
            get
            {
                if(String.IsNullOrEmpty(SearchWord))
                {
                    return Topic;
                }
                return firstTitle;
            }

            set
            {
                if(firstTitle != value)
                {
                    firstTitle = value;
                    NotifyPropertyChanged("FirstTitle");
                }
            }
        }

        private string searchWord;
        public string SearchWord
        {
            get
            {
                return searchWord;
            }

            set
            {
                if (searchWord != value)
                {
                    searchWord = value;
                    NotifyPropertyChanged("SearchWord");
                }
            }
        }

        private string lastTitle;
        public string LastTitle
        {
            get
            {
                return lastTitle;
            }

            set
            {
                if (lastTitle != value)
                {
                    lastTitle = value;
                    NotifyPropertyChanged("LastTitle");
                }
            }
        }

        private RoomStories stories;
        public RoomStories Stories
        {
            get
            {
                return stories;
            }
            set
            {
                if (stories != value)
                {
                    stories = value;
                    NotifyPropertyChanged("BriefText");
                    NotifyPropertyChanged("ShortDate");
                }
            }
        }

        public string BriefText
        {
            get
            {
                if(Stories != null && Stories.Stories != null && Stories.Stories.Count > 0)
                {
                    if(!String.IsNullOrEmpty(Stories.Stories[0].Message))
                    {
                        return HttpUtility.HtmlDecode(Stories.Stories[0].Message.Replace("\n", String.Empty).Replace("\r", String.Empty));
                    }
                    else
                    {
                        return String.Empty;
                    }
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public string ShortDate
        {
            get
            {
                if(Stories != null && Stories.Stories != null && Stories.Stories.Count > 0)
                {
                    System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                    dtDateTime = dtDateTime.AddSeconds(Stories.Stories[0].Created / 1000).ToLocalTime();

                    if(dtDateTime.Year < DateTime.Now.Year)
                    {
                        return dtDateTime.ToString("dd/MM/yy");
                    }

                    return dtDateTime.ToString("dd/MM");
                }
                else
                {
                    return String.Empty;
                }
            }
        }
    }

    public class RoomStory
    {
        public string StoryId { get; set; }
        public long Created { get; set; }
        public long Updated { get; set; }
        public string UserId { get; set; }
        public string Message { get; set; }
        public bool IsSystemStory { get; set; }
    }

    public class RoomStories
    {
        public List<RoomStory> Stories { get; set; }
        public string Cursor { get; set; }
        public int Total { get; set; }
    }
}
